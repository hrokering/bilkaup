using System.Collections.Generic;
using System.Linq;
using System;
using System.Web;
using API.Controllers;
using Bilkaup.Models.DTOModels;
using Bilkaup.Repositories;
using Bilkaup.Services;
using Bilkaup.ElasticSearch;
using Bilkaup.Models.ViewModels;
using Bilkaup.Tests.MockObjects;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Bilkaup.Tests
{
    [TestClass]
    public class CarTests
	{
	    private ICarRepository _repo;
	    private ICarService _carService;
	    private CarController _carController;
	    private MockData _data;
		private ESClientProvider _esClientProvider;
		private IHttpContextAccessor _httpContextAccessor;

		[TestInitialize]    
		public void Initialize()
		{
			_repo = new MockCarRepository();
			_carService = new CarService(_repo);
			_carController = new CarController(_carService, _esClientProvider);
			_carController.ControllerContext = new ControllerContext();
			_carController.ControllerContext.HttpContext = new DefaultHttpContext();
			_data = new MockData();
		}

		// Car test
		// TODO Finish when all car info has been inserted in DB.
		// Testing GET "/api/car"
		[TestMethod]
		public void GetCars()
		{
			// Arrange:

			// Act:
			var response = _carController.GetCars();
			OkObjectResult result = response as OkObjectResult;
			List<CarCardDTO> car = result.Value as List<CarCardDTO>;

			// Assert
			Assert.IsInstanceOfType(response, typeof(OkObjectResult));
			Assert.AreEqual(car.Count(), 2);
		}

		// Testing DELETE api/car/{serialNum}
		[TestMethod]
		public async System.Threading.Tasks.Task SellCarAsync()
		{
			// Arrange			
			// var correctSerialNum = 1;
			var notFoundtSerialNum = 100;
			//var preFailSerialNum = 0;

			// Act
			var notFoundResponse = await _carController.SellCarFunction(notFoundtSerialNum);
			NotFoundResult notFoundResult = notFoundResponse as NotFoundResult;

			// var okResponse = await _carController.SellCarFunction(correctSerialNum);
			// OkResult okResult = okResponse as OkResult;

			// var preConFailedResponse = _carController.SellCarFunction(preFailSerialNum);
			// StatusCodeResult preConFailedResult = await preConFailedResponse as StatusCodeResult;

			// Assert
			Assert.IsInstanceOfType(notFoundResponse, typeof(NotFoundResult));
			// Assert.IsInstanceOfType(okResponse, typeof(OkResult));
			// Assert.IsInstanceOfType(preConFailedResponse, typeof(StatusCodeResult));
		}

		[TestMethod]
		public void GetCarBySerialNum()
		{
			// Arrange
			var illegalSeriaNum = 0;
			var correctSerialNum = 1;
			var notFoundSerialNum = 100;

			// Act
			var illegalRequestResult = _carController.GetCarBySerialNum(illegalSeriaNum);
			BadRequestResult illegalResult = illegalRequestResult as BadRequestResult;

			var badRequestResult = _carController.GetCarBySerialNum(notFoundSerialNum);
			BadRequestResult badResult = badRequestResult as BadRequestResult;

			var okResponse = _carController.GetCarBySerialNum(correctSerialNum);
			OkObjectResult okResult = okResponse as OkObjectResult;

			// Assert
			Assert.IsInstanceOfType(badRequestResult, typeof(BadRequestResult));
			Assert.IsInstanceOfType(okResponse, typeof(OkObjectResult));
		}
		[TestMethod]
		public void GetCarByLicenceNum()
		{
			// Arrange
			var correctLicenceNum = "PEY45";
			var notFoundLicenceNum = "bla";

			// Act

			var notFoundResponse = _carController.GetCarByLicenceNum(notFoundLicenceNum);
			NotFoundResult notFoundResult = notFoundResponse as NotFoundResult;

			var okResponse = _carController.GetCarByLicenceNum(correctLicenceNum);
			OkObjectResult okResult = okResponse as OkObjectResult;

			// Assert
			Assert.IsInstanceOfType(notFoundResponse, typeof(NotFoundResult));
			Assert.IsInstanceOfType(okResponse, typeof(OkObjectResult));
		}
		[TestMethod]
		public async System.Threading.Tasks.Task FindAsync()
		{
			// Arrange
			var term = "";
			var sort = "id";

			// Act

			var okResponse = await _carController.Find(sort, term);
			OkObjectResult okResult = okResponse as OkObjectResult;

			// Assert
			Assert.IsInstanceOfType(okResponse, typeof(OkObjectResult));
		}

		[TestMethod]
		public async System.Threading.Tasks.Task CreateFunctionAsync()
		{
			// Arrange
			CarDetailElasticDTO car = new CarDetailElasticDTO(){ID = 1};

			// Act

			var okResponse = await _carController.CreateFunction(car);
			OkResult okResult = okResponse as OkResult;

			// Assert
			Assert.IsInstanceOfType(okResponse, typeof(OkResult));
		}
		[TestMethod]
		public void GetFilters()
		{
			// Arrange

			// Act
			var okRequestResult = _carController.GetFilters();
			OkObjectResult okResult = okRequestResult as OkObjectResult;

			// Assert
			Assert.IsInstanceOfType(okRequestResult, typeof(OkObjectResult));
		}
		[TestMethod]
		public void GetLicenceNumber()
		{
			// Arrange

			// Act
			var okRequestResult = _carController.GetLicenceNumbersFunction();
			OkObjectResult okResult = okRequestResult as OkObjectResult;

			// Assert
			Assert.IsInstanceOfType(okRequestResult, typeof(OkObjectResult));
		}
		[TestMethod]
		public void GetCarInfo()
		{
			// Arrange

			// Act
			var okRequestResult = _carController.GetCarInfo();
			OkObjectResult okResult = okRequestResult as OkObjectResult;

			// Assert
			Assert.IsInstanceOfType(okRequestResult, typeof(OkObjectResult));
		}

		[TestMethod]
		public void AddCarFunction()
		{
			 
			// Arrange:
			var okCar = _data.okCar;
			CarViewModel invalidCar = null;
			var existingCar = _data.existingCar;

			// Act:
			var okResponse = _carController.AddCarFunction(okCar);
			CreatedAtActionResult okResult = okResponse as CreatedAtActionResult;
			
			var existingResponse = _carController.AddCarFunction(existingCar);
			CreatedAtActionResult existingResult = existingResponse as CreatedAtActionResult;

			var invalidModelStateResponse = _carController.AddCarFunction(invalidCar);
			StatusCodeResult badStatus = invalidModelStateResponse as StatusCodeResult;
			
			// Assert:
		
			Assert.IsInstanceOfType(okResult, typeof(CreatedAtActionResult));
			Assert.IsInstanceOfType(existingResult, typeof(CreatedAtActionResult));
			Assert.IsInstanceOfType(badStatus, typeof(StatusCodeResult));
		}
		[TestMethod]
		public void EditCarFunction()
		{
			 
			// Arrange:
			var okCar = _data.okCar;

			// Act:
			var createdResponse = _carController.EditCarFunction(okCar, 1);
			CreatedAtActionResult createdResult = createdResponse as CreatedAtActionResult;
			
			// Assert:
		
			Assert.IsInstanceOfType(createdResult, typeof(CreatedAtActionResult));
		}
		[TestMethod]
		public void AddCarToElastic()
		{
			 
			// Arrange:
			var serialNum = 1;

			// Act:
			var createdAtActionResponse = _carController.AddCarToElastic(serialNum);
			CreatedAtActionResult createdResult = createdAtActionResponse as CreatedAtActionResult;
			
			// Assert:
		
			Assert.IsInstanceOfType(createdResult, typeof(CreatedAtActionResult));
		}
	}
}
