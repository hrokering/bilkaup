using System;
using System.Collections.Generic;
using System.Linq;
using Bilkaup.Models.EntityModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bilkaup.Repositories;
using Bilkaup.Services;
using Bilkaup.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Bilkaup.Models.DTOModels;
using Bilkaup.Tests.MockObjects;
using API.Controllers;
using Microsoft.AspNetCore.Http;
using Nest;
using System.Threading.Tasks;

namespace Bilkaup.Tests.MockObjects
{
    	public class MockCarRepository : ICarRepository //where T : class
	{
		private static ICollection<Car> _cars;
        private static ICollection<Manufacturer> _manufacturer;
        private static ICollection<Model> _models;
        private static ICollection<ModelType> _modelTypes;
        private static ICollection<FuelType> _fuelTypes;
        private static ICollection<FuelTypeCar> _fuelTypeCars;
        private static ICollection<Transmission> _transmissions;
        private static ICollection<CarSale> _carSales;
        private static ICollection<Drive> _drives;
        private static ICollection<SaleInfo> _saleInfos;
        private static ICollection<Wheel> _wheel;
        private static ICollection<ExtraFeature> _extraFeatures;
        private static ICollection<DriveSteeringInfo> _driveSteeringInfos;
        private static ICollection<Picture> _pictures;


		public MockCarRepository()
		{
			MockData data = new MockData();
			_cars = data.Car;
            _manufacturer = data.Manufacturer;
            _models = data.Model;
            _modelTypes = data.ModelType;
            _fuelTypes = data.FuelType;
            _carSales = data.CarSale;
            _saleInfos = data.SaleInfos;
            _extraFeatures = data.ExtraFeature;
            _driveSteeringInfos = data.DriveSteeringInfo;
            _wheel = data.Wheel;
            _pictures = data.Picture;
                        
		}

		public CarDetailDTO AddCar(CarViewModel car)
        {
            Console.WriteLine("Adding car to database");

            Car c = new Car();

            c.LicenceNumber = car.regNum;
            c.Year = car.year;
            c.ManufacturerID = 1;
            c.ModelID = 2;

            //_db.Cars.Add(c);
            //_db.SaveChanges();
            
            CarDetailDTO result = new CarDetailDTO();
            result.ID = c.ID;
            result.manufacturer = car.manufacturer;
            result.model = car.model;
            result.serialNumber = 1;

            return result;
        }

        public int AddCar(Car car)
        {
            if(car == null)
            {
                return 0;
            }
            
            Console.WriteLine("Adding car to mock database");
            _cars.Add(car);
           // _cars.SaveChanges();

            return car.ID;
        }

        public bool AddDriveSteeringInfoCar(DriveSteeringInfoCar dc)
        {
            return true;
        }

        public int AddExtraFeature(ExtraFeature addExtra)
        {
            throw new NotImplementedException();
        }

        public bool AddExtraFeaturesCars(ExtraFeaturesCar ec)
        {
            return true;
        }

        public bool AddFuelTypeCar(FuelTypeCar fc)
        {
            return true;
        }

        public void addImage(string image, int serialNum)
        {
            throw new NotImplementedException();
        }

        public int AddManufacturer(Manufacturer manufacturer)
        {
            if (manufacturer == null)
            {
                return 0;
            }

            //_cars.SaveChanges();
            _manufacturer.Add(manufacturer);

            return manufacturer.ID;
        }

        public int AddModel(Model model)
        {
            if(model == null)
            {
                return 0;
            }

            _models.Add(model);

            return model.ID;
        }

        public int AddSellerInfo(SaleInfo newInfo)
        {
            if (newInfo == null)
            {
                return 0;
            }

            _saleInfos.Add(newInfo);
           // _db.SaveChanges();

            return 1;
        }

        public bool AddWheelCar(WheelCar wc)
        {
            return true;
        }

        public CarDetailDTO GetCarDetail(int carID, int serialNum)
        {
            var car = (from c in _cars
                        join mf in _manufacturer
                        on c.ManufacturerID equals mf.ID
                        join mod in _models
                        on c.ModelID equals mod.ID
                        where c.ID == carID
                        select new CarDetailDTO
                        {
                            year = "12.12.2012",
                            ID = 1,
                            manufacturer = "",
                            model = "",
                            modelType = "",
                            co2 = 1,
                            color = "",
                            weight = 1,
                            milage = 1,
                            nextCheckup = "",
                            fuelTypes = null,
                            cylinders = 1,
                            cc = 1,
                            injection = true,
                            horsepower = 1,
                            price = 1,
                            engineWeight = 1,
                            transmission = null,
                            drive = "",
                            driveSteering = null,
                            seating = 1,
                            doors = 1,
                            moreInfo = "",
                            status = "",
                            dateSale = DateTime.Now,
                            dateUpdate = DateTime.Now,
                            extraFeatures = null,
                            carsale = new CarSaleBasicDTO
                            {
                                ID = 1,
                                name = "",
                                email = "",
                                phoneNum = "",
                                webpage = "",
                                address = ""
                            },
                            
                            primaryPhotoUrl = "",
                            photosUrl  =  null,
                            hybrid = false,
                            wheels = null,
                            onSite = false,
                            licenceNumber = ""
                        }).SingleOrDefault();

            return car;
        }

        public int GetCarIDBySerial(int serialNum)
        {
            var id = (from si in _saleInfos
                        where si.SerialNum == serialNum
                        select si.CarID).SingleOrDefault();

            return id;
        }

        public GetCarInfoDTO GetCarInfo()
        {
            GetCarInfoDTO info = new GetCarInfoDTO();
            info.wheels = GetWheels();
            info.fuelTypes = GetFuelTypes();
            info.driveSteering = GetDriveSteeringInfos();
            info.extraFeatures = GetExtraFeatures();
            return info;
        }
        public IEnumerable<ExtraFeaturesDTO> GetExtraFeatures()
        {
            var extra = (from e in _extraFeatures
                            select new ExtraFeaturesDTO
                            {
                                id = e.ID,
                                name = e.Name
                            }).ToList();
            return extra;
        }

        /// <summary>
        /// Gets all unsold cars from the database.
        /// </summary>
        /// <returns>
        /// List of CarDetailDTO.
        /// </returns>
        public IEnumerable<CarCardDTO> GetCars()
        {
            return new List<CarCardDTO>
            {
                new CarCardDTO
                {
                    serialNum= 1,
                    manufacturer = "Toyota"
                },
                new CarCardDTO
                {
                    serialNum = 2,
                    manufacturer = "Honda"
                }
            };
        }

        public IEnumerable<DriveSteeringDTO> GetDriveSteeringInfos()
        {
            var driveSteering = (from d in _driveSteeringInfos
                                select new DriveSteeringDTO
                                {
                                    id = d.ID,
                                    name = d.Name
                                }).ToList();
            return driveSteering;
        }

        public FilterDTO GetFilters()
        {
            var filters = new FilterDTO();

            filters.manufacturers = (from m in _manufacturer
                            select new ManufacturerFilterDTO
                            {
                                name = m.Name,
                                selected = false,
                                models = (from mo in _models
                                            where mo.ManufID == m.ID
                                            select new FeatureFilterDTO{
                                                name = mo.Name,
                                                selected = false
                                            }).ToList()
                            }).ToList();
            filters.extraFeatures = (from f in _extraFeatures
                                    select new FeatureFilterDTO{
                                        name = f.Name,
                                        selected = false
                                    }).ToList();
            filters.fuelTypes = (from f in _fuelTypes
                                    select new FeatureFilterDTO{
                                        name = f.Fuel,
                                        selected = false
                                    }).ToList();
            return filters;
        }

        public IEnumerable<FuelTypeDTO> GetFuelTypes()
        {
            var fuelType = (from a in _fuelTypes
                                select new FuelTypeDTO
                                {
                                    id = a.ID,
                                    fuel = a.Fuel
                                }).ToList();
            return fuelType;
        }

        public int GetManufacturerIdByName(string manufacturer)
        {
            var manufacturerId = (from mf in _manufacturer
                                    where mf.Name == manufacturer
                                    select mf.ID
                                ).SingleOrDefault();
            
            if (manufacturerId > 0)
            {
                return manufacturerId;
            }
            else
            {
                return 0;
            }
        }

        public int GetModelIdByName(int manufacturerId, string model)
        {
            var modelId = (from m in _models
                            where m.Name == model
                            && m.ManufID == manufacturerId
                            select m.ID
                        ).SingleOrDefault();
            
            if (modelId > 0)
            {
                return modelId;
            }
            else
            {
                return 0;
            }
        }

        public IEnumerable<WheelDTO> GetWheels()
        {
            var wheel = (from a in _wheel
                            select new WheelDTO
                            {
                                id = a.ID,
                                name = a.Name
                            }).ToList();
            return wheel;
        }

        public bool SellCar(SaleInfo sold)
        {
            var carInfo = _saleInfos.SingleOrDefault(si => si.SerialNum == sold.SerialNum);

            if (carInfo == null)
            {
                return false;
            }
            else
            {
                carInfo.DateOfSale = sold.DateOfSale;
                //_db.SaveChanges();
                return true;
            }
        }
        public void addImage(string image, int serialNum, bool primaryStatus)
        {
            Picture picture = new Picture();
            var primary =  (from p in _pictures
                                    where p.CarSerialNum == serialNum
                                    select p
                                ).SingleOrDefault();
            picture.CarSerialNum = serialNum;
            picture.Link = "https://bilkaup.blob.core.windows.net/" + serialNum.ToString() + "/" + image;
            picture.Primary = primaryStatus;
            //_db.Pictures.Add(picture);
            //_db.SaveChanges();
        }

        public CarViewModel GetCarForEdit(int carID)
        {
            var car = (from c in _cars
                        join mf in _manufacturer
                        on c.ManufacturerID equals mf.ID
                        join mod in _models
                        on c.ModelID equals mod.ID
                        where c.ID == carID
                        select new CarViewModel
                        {
                            id = c.ID
                        }).FirstOrDefault();

            return car;
        }

        public IEnumerable<string> GetLicenceNumbers()
        {
            var numbers = (from num in _cars
                            select num.LicenceNumber).ToList();
            
            return numbers;
        }

        public CarViewModel GetCarForEditExisting(int serialNum, int carId)
        {
            throw new NotImplementedException();
        }

        public int GetIDByLicenceNum(string licenceNum)
        {
            var id = (from c in _cars
                        where c.LicenceNumber == licenceNum
                        select c.ID).FirstOrDefault();
            
            return id;
        }

        public bool EditCarTable(int id, CarViewModel editCar)
        {
            return true;
        }

        public bool DeleteWheels(int carID)
        {
            return true;
        }

        public bool DeleteFuelTypes(int carID)
        {
            return true;
        }

        public bool DeleteDriveSteering(int carID)
        {
            return true;
        }

        public bool DeleteExtraFeatures(int carID)
        {
            return true;
        }

        public AddCarResultDTO EditSellerTable(CarViewModel car, int serialNum)
        {
            AddCarResultDTO result = new AddCarResultDTO()
            {
                carId = car.id,
                carSerialNum = serialNum
            };
            return result;
        }

        public Task<bool> deleteFromElasticsearch(int serialNum)
        {
            throw new NotImplementedException();
        }

        public async Task<List<CarDetailElasticDTO>> findCarsInElasticsearch(string sort, string term = null)
        {
            
            var cars =  (from c in _cars
                                    select new CarDetailElasticDTO
                                    {
                                        ID = c.ID
                                    }
                                ).ToList();
            return cars;
        }

        public Task<bool> deleteBlob(string serialNum)
        {
            throw new NotImplementedException();
        }

        public Task createBlob(string serialNum, IFormFile image)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> createInElasticsearch(CarDetailElasticDTO car)
        {
            return true;
        }
    }
}