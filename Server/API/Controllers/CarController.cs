﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Bilkaup.Controllers;
using Bilkaup.ElasticSearch;
using Bilkaup.Models;
using Bilkaup.Models.DTOModels;
using Bilkaup.Models.ViewModels;
using Bilkaup.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Nest;

namespace API.Controllers
{
    /// <summary>
    /// The controller that receives all the Http requests for the Cars
    /// </summary>
    /// <remarks>
    /// Contains the GET, POST, PUT, DELETE requests for the Cars.
    /// Each type of request is under it's own category.
    /// </remarks>
    [Route("api/[controller]")]
    public class CarController : Controller
    {
        private readonly ICarService _carService;
        private ESClientProvider _esClientProvider;
        private readonly string CARSALE = "Carsale";
        
        public CarController(ICarService carService, ESClientProvider esClientProvider)
        {
            _carService = carService;
            _esClientProvider = esClientProvider;
        }

        /*
        * --------------------------------- GET REQUESTS ---------------------------------
        */

        /// <summary>
        /// GET api/car
        /// Gets all the unsold cars in the database.
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK if all goes well.
        /// </returns>
        [HttpGet("")]
        public IActionResult GetCars()
        {
            var cars = _carService.GetCars();

            return Ok(cars);
        }

        /// <summary>
        /// GET api/car/{id}
        /// Gets a specific car from the database by it's ID
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK if the car exists.
        /// 400 - Bad Request if the car does not exist.
        /// </returns>
        [HttpGet("{serialNum:int}")]
        public IActionResult GetCarBySerialNum(int serialNum)
        {
            if (serialNum < 1)
            {
                return BadRequest();
            }

            var car = _carService.GetCarBySerialNum(serialNum);

            if (car == null)
            {
                return BadRequest();
            }

            return Ok(car);
        }

        /// <summary>
        /// GET api/car/{id}
        /// Gets a specific car from the database by it's ID
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK if the car exists.
        /// 400 - Bad Request if the car does not exist.
        /// </returns>
        [HttpGet("editExisting/{carsaleId}/{serialNum}")]
        public IActionResult GetCarBySerialNumFunction(int carsaleId, int serialNum)
        {
            if (serialNum == 0)
            {
                return BadRequest();
            }

            var car = _carService.GetCarBySerialNumEdit(serialNum);
            if (car.carSaleId != carsaleId)
            {
                return Unauthorized();
            }
            if (car == null)
            {
                return BadRequest();
            }

            return Ok(car);
        }

        /// <summary>
        /// GET api/car/{id}
        /// Gets a specific car from the database by it's ID
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK if the car exists.
        /// 400 - Bad Request if the car does not exist.
        /// </returns>
        [HttpGet("edit/{licenceNum}")]
        public IActionResult GetCarByLicenceNum(string licenceNum)
        {
            if (licenceNum == null)
            {
                return BadRequest();
            }

            var car = _carService.GetCarByLicenceNum(licenceNum);

            if (car == null)
            {
                return NotFound();
            }

            return Ok(car);
        }

        /// <summary>
        /// GET api/car/find
        /// Gets cars from elasticsearch database with sort and search term
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK with the cars that fit the term
        /// 400 - Bad Request if something went wrong
        /// </returns>
        [HttpGet("find")]
        public async Task<IActionResult> Find([FromHeader] string sort, string term = null)
        {
            var response = await _carService.findCarsInElasticsearch(sort, term);
            if(response != null)
            {
                return Ok(response); //Json(response.Documents);
            }
            else
            {
                // throw new InvalidOperationException(response.DebugInformation);
                return BadRequest();
            }
        }
        
        /// <summary>
        /// GET api/car/filters
        /// Gets search filters from the database
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK with the filters
        /// </returns>
        [HttpGet("filters")]
        public IActionResult GetFilters()
        {
            var filters = _carService.GetFilters();

            return Ok(filters);
        }

        /// <summary>
        /// GET api/car/carInfo
        /// Get car info like wheel, fueltype, drive steering
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK with informations about the car
        /// </returns>
        [HttpGet("carInfo")]
        public IActionResult GetCarInfo()
        {
            var carInfo = _carService.GetCarInfo();

            return Ok(carInfo);
        }

        /// <summary>
        /// GET api/car/regNum
        /// Gets all the licence numbers of cars in the database
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK with the list
        /// </returns>
        [HttpGet("regNum")]
        public IActionResult GetLicenceNumbers()
        {
            if (HttpContext.Request.Cookies["User"] == CARSALE)
            {
                return GetLicenceNumbersFunction();
            }

            return Unauthorized();
        }

        public IActionResult GetLicenceNumbersFunction()
        {
            //Tested
            var numbers = _carService.GetLicenceNumbers();

            return Ok(numbers);
        }
        
        /*
        * --------------------------------- POST REQUESTS ---------------------------------
        */
        
        /// <summary>
        /// POST api/car/Create
        /// Creates a car in the database
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK if the car was successfully created
        /// </returns>
        public async Task<IActionResult> Create(CarDetailElasticDTO car)
        {
            //Tested
            if (HttpContext.Request.Cookies["User"] == CARSALE)
            {
                return await CreateFunction(car);
            }
            return Unauthorized();
        }
        public async Task<IActionResult> CreateFunction(CarDetailElasticDTO car)
        {
            var res = await _carService.createInElasticsearch(car);
            if (res == false)
            {
                return BadRequest();
            }

            return Ok();
        }

        /// <summary>
        /// POST api/car/{serialNum}/images
        /// Creates images for a car in Blob storage
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 201 - Created with the serial number of the car
        /// </returns>
        [HttpPost("{serialNum:int}/images")]
        public async Task<IActionResult> Images([FromHeader] string primary, IFormFile image, int serialNum)
        {
            //Not tested, hard to mock IFormFile
            if (HttpContext.Request.Cookies["User"] == CARSALE)
            {
                if (image == null)
                {
                    throw new ArgumentNullException(nameof(image));
                }

                if (image.Length == 0) 
                {
                    throw new Exception("File is empty");
                }
                await _carService.createBlob(serialNum.ToString(), image);

                string imageString = "https://bilkaup.blob.core.windows.net/" + serialNum.ToString() + "/" + image.FileName;
                bool primaryStatus = false;
                if (image.FileName == primary)
                {
                    primaryStatus = true;
                }

                _carService.addImage(image.FileName, serialNum, primaryStatus);

                return CreatedAtAction("Registered", serialNum);
            }

            return Unauthorized();
        }

        /// <summary>
        /// POST api/car/
        /// Creates a car in sql database
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 201 - Created with the serial number of the car
        /// </returns>
        [HttpPost(""), DisableRequestSizeLimit]
        //[AllowAnonymous]
        public IActionResult AddCar([FromBody] CarViewModel newCar)
        {
            // Check if logged in user is authenticaded as Carsale
            if (HttpContext.Request.Cookies["User"] == CARSALE)
            {
                return AddCarFunction(newCar);
            }

            return Unauthorized();
        }

        public IActionResult AddCarFunction(CarViewModel newCar)
        {   
            if (newCar == null)
            {
                return StatusCode(412);
            }

            if (!ModelState.IsValid)
            {
                return StatusCode(412);
            }
            
            if (newCar.id > 0)
            {
                var serialNum = _carService.AddExistingCar(newCar); 
                return CreatedAtAction("Added to database", serialNum);
            }
            else
            {
                var registeredCar = _carService.AddCar(newCar);
                return CreatedAtAction("Added to database", registeredCar.carSerialNum);
            }
        }

        /// <summary>
        /// POST api/car/AddCarToElastic{serialNum}
        /// Creates a car in Elasticsearch database
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 201 - Created with the serial number of the car if the car was created
        /// Bad Request if something went wrong
        /// </returns>
        [HttpPost("AddCarToElastic/{serialNum:int}")]
        public IActionResult AddCarToElastic(int serialNum)
        {
            var car = _carService.GetCarBySerialNum(serialNum);
            var carForElastic = _carService.getCarForElasticsearch(car);
            var elasticResult = Create(carForElastic);

            if (elasticResult != null)
            {
                return CreatedAtAction("Added to Elasticsearch", car.serialNumber);
            }

            return BadRequest();
        }

        /*
        * --------------------------------- PUT REQUESTS ---------------------------------
        */

         /// <summary>
        /// PUT api/car/edit/{serialNum}
        /// Edits information about a car
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 201 - Created with the serial number of the car
        /// </returns>
        [HttpPut("edit/{serialNum:int}")]
        public IActionResult EditCar([FromBody] CarViewModel newCar, int serialNum)
        {
            // Check if logged in user is authenticaded as Carsale
            if (HttpContext.Request.Cookies["User"] == CARSALE)
            {
                return EditCarFunction(newCar, serialNum);
            }

            return Unauthorized();
        }

         public IActionResult EditCarFunction(CarViewModel newCar, int serialNum)
        {   
            if (newCar == null || !ModelState.IsValid)
            {
                return StatusCode(412);
            }
            
            // TODO: Check if the car exists, and if it exists call EditCar, not AddCar...
            // as car cannot be twice in the DB

            var registeredCar = _carService.EditCar(newCar, serialNum);

            return CreatedAtAction("Registered", registeredCar.carSerialNum);
        }

        /*
        * --------------------------------- DELETE REQUESTS ---------------------------------
        */

        /// <summary>
        /// DELETE api/car/{serialNum}
        /// Deletes a car
        /// </summary>
        /// <returns>
        /// An IActionResult.
        /// 200 - OK if the car was deleted successfully
        /// 412 - Precodition failed if the serial number of the car is invalid
        /// 400 - Bad Request is something failed
        /// </returns>
        [HttpDelete("{serialNum:int}")]
        public async Task<IActionResult> SellCar(int serialNum)
        {
            if (HttpContext.Request.Cookies["User"] == CARSALE)
            {
                return await SellCarFunction(serialNum);
            }

            return Unauthorized();
        }
        
        public async Task<IActionResult> SellCarFunction(int serialNum) 
        {
            
            if (serialNum > 0)
            {
                var car = _carService.GetCarBySerialNum(serialNum);

                if (car == null)
                {
                    return NotFound();
                }
                else
                {
                    var cars = _carService.SellCar(serialNum);
                    
                    var res = await _carService.deleteFromElasticsearch(serialNum);

                    if(res == true)
                    {
                        if(car.primaryPhotoUrl != null)
                        {
                            var response = await _carService.deleteBlob(serialNum.ToString());
                            
                            if(response == true)
                            {
                                return Ok();
                            }
                        }
                        return Ok();
                    }
                }
                return BadRequest();
            }
            else
            {
                return StatusCode(412);
            }
        }
    }
}