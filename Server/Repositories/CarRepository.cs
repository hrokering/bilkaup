﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bilkaup.Models.DTOModels;
using Bilkaup.Models.EntityModels;
using Bilkaup.Models.ViewModels;
using Bilkaup.ElasticSearch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace Bilkaup.Repositories
{
    /// <summary>
    /// Connects everything that has to do with cars to the database.
    /// </summary>
    /// <remarks>
    /// Every single function that has to do with the cars goes through this controller.
    /// </remarks>
    public class CarRepository : ICarRepository
    {
        private readonly ApplicationDbContext _db;
        private ESClientProvider _esClientProvider;
        private readonly IConfiguration _configuration;


        public CarRepository(ApplicationDbContext db, ESClientProvider esClientProvider, IConfiguration configuartion)
        {
            _db = db;
            _esClientProvider = esClientProvider;
            _configuration = configuartion;
        }

        /// <summary>
        /// Adds a car to db
        /// </summary>
        /// <param name=”car”>
        /// Parameter car requires a Car argument.
        /// </param>
        /// <returns>
        /// car Id
        /// </returns>
        public int AddCar(Car car)
        {
            if (car == null)
            {
                return -1;
            }
            
            _db.Cars.Add(car);
            _db.SaveChanges();

            return car.ID;
        }

        /// <summary>
        /// Adds wheel of a car to db
        /// </summary>
        /// <param name=”wc”>
        /// Parameter wc requires a WheelCar argument.
        /// </param>
        /// <returns>
        /// False - if wc is null
        /// True - if wheelcar was added successfully
        /// </returns>
        public bool AddWheelCar(WheelCar wc)
        {
            if (wc == null)
            {
                return false;
            }
            
            _db.WheelCars.Add(wc);
            _db.SaveChanges();

            return true;
        }

        /// <summary>
        /// Adds fuel type of a car to db
        /// </summary>
        /// <param name=”fc”>
        /// Parameter fc requires a FuelTypeCar argument.
        /// </param>
        /// <returns>
        /// False - if fc is null
        /// True - if fuelTypeCar was added successfully
        /// </returns>
        public bool AddFuelTypeCar(FuelTypeCar fc)
        {
            if (fc == null)
            {
                return false;
            }
            
            _db.FuelTypeCars.Add(fc);
            _db.SaveChanges();

            return true;
        }

        /// <summary>
        /// Adds drive steering informations of a car to db
        /// </summary>
        /// <param name=”dc”>
        /// Parameter dc requires a DriveSteeringInfoCar argument.
        /// </param>
        /// <returns>
        /// False - if dc is null
        /// True - if DriveSteeringInfoCar was added successfully
        /// </returns>
        public bool AddDriveSteeringInfoCar(DriveSteeringInfoCar dc)
        {
            if (dc == null)
            {
                return false;
            }
            
            _db.DriveSteeringInfoCars.Add(dc);
            _db.SaveChanges();

            return true;
        }

        /// <summary>
        /// Adds extra feature of a car to db
        /// </summary>
        /// <param name=”fc”>
        /// Parameter fc requires a ExtraFeaturesCar argument.
        /// </param>
        /// <returns>
        /// False - if fc is null
        /// True - if DriveSteeringInfoCar was added successfully
        /// </returns>
        public bool AddExtraFeaturesCars(ExtraFeaturesCar ec)
        {
            if (ec == null)
            {
                return false;
            }
            
            _db.ExtraFeaturesCars.Add(ec);
            _db.SaveChanges();

            return true;
        }

        /// <summary>
        /// Adds extra feature of a car to db
        /// </summary>
        /// <param name=”addExtra”>
        /// Parameter addExtra requires a ExtraFeature argument.
        /// </param>
        /// <returns>
        /// -1 - if addExtra is null
        /// Id of the extra feature if it was added successfully
        /// </returns>
        public int AddExtraFeature(ExtraFeature addExtra)
        {
            if (addExtra == null)
            {
                return -1;
            }

            _db.ExtraFeatures.Add(addExtra);
            _db.SaveChanges();

            return addExtra.ID;
        }


        /// <summary>
        /// Adds extra feature of a car to db
        /// </summary>
        /// <param name=”addExtra”>
        /// Parameter addExtra requires a ExtraFeature argument.
        /// </param>
        /// <returns>
        /// -1 - if addExtra is null
        /// Id of the extra feature if it was added successfully
        public IEnumerable<CarCardDTO> GetCars()
        {
           var cars = (from c in _db.Cars
                            join m in _db.Manufacturers
                            on c.ManufacturerID equals m.ID
                            join t in _db.Models
                            on c.ModelID equals t.ID
                            select new CarCardDTO
                            {
                                serialNum = c.ID,
                                model = t.Name,
                                manufacturer = m.Name
                            }).ToList();
            return cars;
        }

        /// <summary>
        /// Gets informations about a car
        /// </summary>
        /// <returns>
        /// -1 - if addExtra is null
        /// Id of the extra feature if it was added successfully
        public GetCarInfoDTO GetCarInfo()
        {
            GetCarInfoDTO info = new GetCarInfoDTO();
            info.wheels = GetWheels();
            info.fuelTypes = GetFuelTypes();
            info.driveSteering = GetDriveSteeringInfos();
            info.extraFeatures = GetExtraFeatures();
            return info;
        }

        /// <summary>
        /// Gets extra features
        /// </summary>
        /// <returns>
        /// List of extra features
        public IEnumerable<ExtraFeaturesDTO> GetExtraFeatures()
        {
            var extra = (from e in _db.ExtraFeatures
                            select new ExtraFeaturesDTO
                            {
                                id = e.ID,
                                name = e.Name
                            }).ToList();
            return extra;
        }

        /// <summary>
        /// Gets wheels
        /// </summary>
        /// <returns>
        /// List of wheels
        private IEnumerable<WheelDTO> GetWheels()
        {
            var wheel = (from a in _db.Wheels 
                            select new WheelDTO
                            {
                                id = a.ID,
                                name = a.Name
                            }).ToList();
            return wheel;
        }

        /// <summary>
        /// Gets fuel types
        /// </summary>
        /// <returns>
        /// List of fuel types
        private IEnumerable<FuelTypeDTO> GetFuelTypes()
        {
            var fuelType = (from a in _db.FuelTypes
                                select new FuelTypeDTO
                                {
                                    id = a.ID,
                                    fuel = a.Fuel
                                }).ToList();
            return fuelType;
        }
         
         /// <summary>
        /// Gets Drive Steering Infos
        /// </summary>
        /// <returns>
        /// List of drive steering infos
        private IEnumerable<DriveSteeringDTO> GetDriveSteeringInfos()
        {
            var driveSteering = (from d in _db.DriveSteeringInfos
                                select new DriveSteeringDTO
                                {
                                    id = d.ID,
                                    name = d.Name
                                }).ToList();
            return driveSteering;
        }
        
        /// <summary>
        /// Gets Id of manufacturer by its name
        /// </summary>
        /// <param name=”manufacturer”>
        /// Parameter manufacturer requires a string argument.
        /// </param>
        /// <returns>
        /// manufacturor id if manufactursr exists
        /// 0 if the manufacturer does not exist
        public int GetManufacturerIdByName(string manufacturer)
        {
            var manufacturerId = (from mf in _db.Manufacturers
                                    where mf.Name == manufacturer
                                    select mf.ID
                                ).SingleOrDefault();
            
            if (manufacturerId > 0)
            {
                return manufacturerId;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets Id of model by its name
        /// </summary>
        /// <param name=”model”>
        /// Parameter model requires a string argument.
        /// </param>
        /// <returns>
        /// model id if manufactursr exists
        /// 0 if the model does not exist
        public int GetModelIdByName(int manufacturerId, string model)
        {
            var modelId = (from m in _db.Models
                            where m.Name == model
                            && m.ManufID == manufacturerId
                            select m.ID
                        ).SingleOrDefault();
            
            if (modelId > 0)
            {
                return modelId;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Adds a model to db
        /// </summary>
        /// <param name=”model”>
        /// Parameter model requires a Model argument.
        /// </param>
        /// <returns>
        /// manufacturor id if model is not null
        /// 0 if the model is null
        public int AddModel(Model model)
        {
            if (model == null)
            {
                return 0;
            }

            _db.Models.Add(model);
            _db.SaveChanges();

            return model.ID;
        }

        /// <summary>
        /// Adds a manufacturor into db
        /// </summary>
        /// <param name=”manufacturer”>
        /// Parameter manufacturer requires a Manufacturer argument.
        /// </param>
        /// <returns>
        /// manufacturor id if manufacturer was created
        /// 0 if manufacturer was null
        public int AddManufacturer(Manufacturer manufacturer)
        {
            if (manufacturer == null)
            {
                return 0;
            }

            _db.Manufacturers.Add(manufacturer);
            _db.SaveChanges();

            return manufacturer.ID;
        }

        /// <summary>
        /// Gets all details about a car
        /// </summary>
        /// <param name=”carID”>
        /// Parameter carID requires a int argument.
        /// </param>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires a int argument.
        /// </param>
        /// <returns>
        /// the car if the car exists
        /// null if the car does not exist
        public CarDetailDTO GetCarDetail(int carID, int serialNum)
        {
            var car = (from c in _db.Cars
                        join mf in _db.Manufacturers
                        on c.ManufacturerID equals mf.ID
                        join mod in _db.Models
                        on c.ModelID equals mod.ID
                        where c.ID == carID
                        select new CarDetailDTO
                        {
                            ID = c.ID,
                            manufacturer = mf.Name,
                            model = mod.Name,
                            modelType = (from modT in _db.ModelTypes
                                        where modT.ModelID == c.ModelID
                                        && modT.ManufID == c.ManufacturerID
                                        select modT.Name).SingleOrDefault(),
                            co2 = c.CO2,
                            color = c.Color,
                            weight = c.Weight,
                            year = c.Year,
                            milage = c.Milage,
                            nextCheckup = c.NextCheckup,
                            fuelTypes = (from fu in _db.FuelTypeCars
                                        join fc in _db.FuelTypes
                                        on fu.FuelTypeID equals fc.ID
                                        where fu.CarID == serialNum
                                        select fc.Fuel).ToList(),
                            wheels = (from wh in _db.WheelCars
                                        join wc in _db.Wheels
                                        on wh.WheelID equals wc.ID
                                        where wh.CarID == serialNum
                                        select wc.Name).ToList(),
                            cylinders = c.Cylinders,
                            cc = c.CC,
                            injection = c.Injection,
                            horsepower = c.Horsepower,
                            transmission = (from trans in _db.Transmissions
                                            where trans.ID == c.TransmissionID
                                            select trans.Name).SingleOrDefault(),
                            extraFeatures = (from ex in _db.ExtraFeatures
                                                join ec in _db.ExtraFeaturesCars
                                                on ex.ID equals ec.ExtraFeaturesID
                                                where ec.CarID == serialNum
                                                select ex.Name).ToList(),
                            drive = (from drive in _db.Drives
                                    where drive.ID == c.DriveID
                                    select drive.Name).SingleOrDefault(),
                            seating = c.Seating,
                            doors = c.Doors,
                            dateSale = (from si in _db.SaleInfos
                                        where si.SerialNum == serialNum
                                        select si.DateOnSale).SingleOrDefault(),
                            dateUpdate = (from si in _db.SaleInfos
                                        where si.SerialNum == serialNum
                                        select si.DateOfUpdate).SingleOrDefault(),
                            carsale = (from cs in _db.CarSales
                                        join si in _db.SaleInfos
                                        on cs.ID equals si.CarSaleID
                                        where si.SerialNum == serialNum
                                        select new CarSaleBasicDTO
                                        {
                                            ID = cs.ID,
                                            name = cs.Name,
                                            email = cs.Email,
                                            phoneNum = cs.PhoneNum,
                                            webpage = cs.Webpage,
                                            address = cs.Address
                                        }).SingleOrDefault(),
                            
                            price = (from si in _db.SaleInfos
                                        where si.SerialNum == serialNum
                                        select si.Price).SingleOrDefault(),
                            hybrid = c.Hybrid,
                            primaryPhotoUrl = (from pi in _db.Pictures
                                        where pi.CarSerialNum == serialNum
                                        && pi.Primary == true
                                        select pi.Link).FirstOrDefault(),
                            photosUrl = (from pi in _db.Pictures
                                        where pi.CarSerialNum == serialNum
                                        select pi.Link).ToList(),
                            serialNumber = serialNum,
                            driveSteering = (from d in _db.DriveSteeringInfos
                                            join ds in _db.DriveSteeringInfoCars
                                            on d.ID equals ds.DriveSteeringID
                                            where ds.CarID == serialNum
                                            select d.Name).ToList(),
                            onSite = (from si in _db.SaleInfos
                                        where si.SerialNum == serialNum
                                        select si.OnSite).SingleOrDefault(),
                            licenceNumber = c.LicenceNumber

                        }).SingleOrDefault();

            return car;
        }

        /// <summary>
        /// Adds informations about seller into db
        /// </summary>
        /// <param name=”newInfo”>
        /// Parameter newInfo requires a SaleInfo argument.
        /// </param>
        /// <returns>
        /// Seller info serial number if model is not null
        /// 0 if the model is null
        public int AddSellerInfo(SaleInfo newInfo)
        {
            if (newInfo == null)
            {
                return 0;
            }

            _db.SaleInfos.Add(newInfo);
            _db.SaveChanges();

            return newInfo.SerialNum;
        }

        /// <summary>
        /// Gets filters for search from db
        /// </summary>
        /// <returns>
        /// FilterDTO with all filters
        public FilterDTO GetFilters()
        {
            var filters = new FilterDTO();

            filters.manufacturers = (from m in _db.Manufacturers
                            select new ManufacturerFilterDTO
                            {
                                name = m.Name,
                                selected = false,
                                models = (from mo in _db.Models
                                            where mo.ManufID == m.ID
                                            select new FeatureFilterDTO{
                                                name = mo.Name,
                                                selected = false
                                            }).ToList()
                            }).ToList();
            filters.extraFeatures = (from f in _db.ExtraFeatures
                                    select new FeatureFilterDTO{
                                        name = f.Name,
                                        selected = false
                                    }).ToList();
            filters.fuelTypes = (from f in _db.FuelTypes
                                    select new FeatureFilterDTO{
                                        name = f.Fuel,
                                        selected = false
                                    }).ToList();
            return filters;
        }
        
        public int GetCarIDBySerial(int serialNum)
        {
            var id = (from si in _db.SaleInfos
                        where si.SerialNum == serialNum
                        select si.CarID).SingleOrDefault();

            return id;
        }

        /// <summary>
        /// Marks car as sold with date
        /// </summary>
        /// <param name=”sold”>
        /// Parameter sold requires a SaleInfo object argument.
        /// </param>
        /// <returns>
        /// false - If the car does not exist
        /// true - If the car was changed correctly
        /// </returns>
        public bool SellCar(SaleInfo sold)
        {
            var carInfo = _db.SaleInfos.SingleOrDefault(si => si.SerialNum == sold.SerialNum);

            if (carInfo == null)
            {
                return false;
            }
            else
            {
                carInfo.DateOfSale = sold.DateOfSale;
                _db.SaveChanges();
                
                return true;
            }
        }

        /// <summary>
        /// Gets all licence numbers from database
        /// </summary>
        /// <returns>
        /// List of strings (licence numbers)
        /// </returns>
        public IEnumerable<string> GetLicenceNumbers()
        {
            // Tested
            var numbers = (from num in _db.Cars
                            select num.LicenceNumber).ToList();
            
            return numbers;
        }

        /// <summary>
        /// Gets the ID of a car by it's licence number
        /// <summary>
        /// <param name=”licenceNum”>
        /// Parameter licenceNum requires a string argument.
        /// </param>
        /// <returns>
        /// The ID number of the car
        /// </returns>
        public int GetIDByLicenceNum(string licenceNum)
        {
            var id = (from c in _db.Cars
                        where c.LicenceNumber == licenceNum
                        select c.ID).FirstOrDefault();
            
            return id;
        }
        /// <summary>
        /// Gets informations about a car
        /// <summary>
        /// <param name=”carID”>
        /// Parameter carID requires a int argument.
        /// </param>
        /// <returns>
        /// CarViewModel with the information about the car if it exists
        /// Null, if the car does not exist
        /// </returns>
        public CarViewModel GetCarForEdit(int carID)
        {
             var car = (from c in _db.Cars
                        join mf in _db.Manufacturers
                        on c.ManufacturerID equals mf.ID
                        join mod in _db.Models
                        on c.ModelID equals mod.ID
                        where c.ID == carID
                        select new CarViewModel
                        {
                            id = c.ID,
                            manufacturer = mf.Name,
                            model = mod.Name,
                            subType = (from modT in _db.ModelTypes
                                        where modT.ModelID == c.ModelID
                                        && modT.ManufID == c.ManufacturerID
                                        select modT.Name).SingleOrDefault(),
                            regNum = c.LicenceNumber,
                            co2 = c.CO2,
                            color = c.Color,
                            weight = c.Weight,
                            status = c.Status,
                            year = c.Year,
                            driven = c.Milage,
                            nextCheckUp = c.NextCheckup,
                            fuelType = (from fu in _db.FuelTypeCars
                                        join fc in _db.FuelTypes
                                        on fu.FuelTypeID equals fc.ID
                                        where fu.CarID == carID
                                        select fc.ID).ToList(),
                            wheel = new List<int>(),
                            cylinders = c.Cylinders,
                            cc = c.CC,
                            injection = c.Injection,
                            horsepower = c.Horsepower,
                            transmission = c.TransmissionID,
                            drive = c.DriveID,
                            seating = c.Seating,
                            doors = c.Doors,
                            driveSteering = (from ds in _db.DriveSteeringInfoCars
                                                join dsc in _db.DriveSteeringInfos
                                                on ds.DriveSteeringID equals dsc.ID
                                                where ds.CarID == carID
                                                select dsc.ID).ToArray(),
                            extraFeatures = new List<ExtraFeatureViewModel>(),
                            photosUrl = new List<string>()
                        }).FirstOrDefault();

            return car;
        }

        /// <summary>
        /// Gets the ID of a car by it's licence number
        /// <summary>
        /// <param name=”licenceNum”>
        /// Parameter licenceNum requires a string argument.
        /// </param>
        /// <returns>
        /// The ID number of the car
        /// </returns>
        public CarViewModel GetCarForEditExisting(int serialNum, int carId)
        {
             var car = (from c in _db.Cars
                        join mf in _db.Manufacturers
                        on c.ManufacturerID equals mf.ID
                        join mod in _db.Models
                        on c.ModelID equals mod.ID
                        where c.ID == carId 
                        select new CarViewModel
                        {
                            id = c.ID,
                            manufacturer = mf.Name,
                            model = mod.Name,
                            subType = (from modT in _db.ModelTypes
                                        where modT.ModelID == c.ModelID
                                        && modT.ManufID == c.ManufacturerID
                                        select modT.Name).SingleOrDefault(),
                            regNum = c.LicenceNumber,
                            co2 = c.CO2,
                            color = c.Color,
                            weight = c.Weight,
                            status = c.Status,
                            year = c.Year,
                            driven = c.Milage,
                            nextCheckUp = c.NextCheckup,
                            fuelType = (from fu in _db.FuelTypeCars
                                        join fc in _db.FuelTypes
                                        on fu.FuelTypeID equals fc.ID
                                        where fu.CarID == serialNum
                                        select fc.ID).ToList(),
                            wheel = (from wh in _db.WheelCars
                                        join wc in _db.Wheels
                                        on wh.WheelID equals wc.ID
                                        where wh.CarID == serialNum
                                        select wc.ID).ToList(),
                            cylinders = c.Cylinders,
                            cc = c.CC,
                            injection = c.Injection,
                            horsepower = c.Horsepower,
                            transmission = (from trans in _db.Transmissions
                                            where trans.ID == c.TransmissionID
                                            select trans.ID).SingleOrDefault(),
                            drive = (from drive in _db.Drives
                                        where drive.ID == c.DriveID
                                        select drive.ID).SingleOrDefault(),
                            seating = c.Seating,
                            doors = c.Doors,
                            driveSteering = (from ds in _db.DriveSteeringInfoCars
                                                join dsc in _db.DriveSteeringInfos
                                                on ds.DriveSteeringID equals dsc.ID
                                                where ds.CarID == serialNum
                                                select dsc.ID).ToArray(),
                            extraFeatures = (from efc in _db.ExtraFeaturesCars
                                                join ef in _db.ExtraFeatures
                                                on efc.ExtraFeaturesID equals ef.ID
                                                where efc.CarID == serialNum
                                                select new ExtraFeatureViewModel
                                                {
                                                    id = efc.ExtraFeaturesID,
                                                    name = ef.Name
                                                }).ToList(),
                            price = (from si in _db.SaleInfos
                                        where si.SerialNum == serialNum
                                        select si.Price).SingleOrDefault(),
                            onSite = (from si in _db.SaleInfos
                                        where si.SerialNum == serialNum
                                        select si.OnSite).SingleOrDefault(),
                            carSaleId = (from si in _db.SaleInfos
                                        where si.SerialNum == serialNum
                                        select si.CarSaleID).SingleOrDefault(),
                            photosUrl = (from p in _db.Pictures
                                        where p.CarSerialNum == serialNum
                                        select p.Link).ToList()
                        }).FirstOrDefault();

            return car;
        }

        /// <summary>
        /// Edits seller informations of a car in db
        /// </summary>
        /// <param name=”car”>
        /// Parameter car requires a CarViewModel argument.
        /// </param>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// AddCarResultDTO with carId and carSerialNum
        /// 0 if the seller does not exist
        public AddCarResultDTO EditSellerTable(CarViewModel car, int serialNum)
        {
            var sellerCar = (from s in _db.SaleInfos
                                where s.SerialNum == serialNum
                                select s).SingleOrDefault();
            if(sellerCar != null)
            {
                sellerCar.DateOfUpdate = DateTime.Now;
                //sellerCar.OfferPrice = car.offerPrice;
                sellerCar.OnSite = car.onSite;
                sellerCar.Price = car.price;
                _db.SaveChanges();
                AddCarResultDTO result = new AddCarResultDTO()
                {
                    carId = car.id,
                    carSerialNum = serialNum
                };
                return result;
            }
            return null;
            
        }
        
        /// <summary>
        /// Adds image to Blob storate
        /// </summary>
        /// <param name=”image”>
        /// Parameter image requires a string argument.
        /// </param>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <param name=”primaryStatus”>
        /// Parameter primaryStatus requires a bool argument.
        /// </param>
        public void addImage(string image, int serialNum, bool primaryStatus)
        {
            Picture picture = new Picture();
            picture.CarSerialNum = serialNum;
            picture.Link = "https://bilkaup.blob.core.windows.net/" + serialNum.ToString() + "/" + image;
            picture.Primary = primaryStatus;
            _db.Pictures.Add(picture);
            _db.SaveChanges();
        }

        /// <summary>
        /// Edits a car in db
        /// </summary>
        /// <param name=”id”>
        /// Parameter id requires an int argument.
        /// </param>
        /// <param name=”car”>
        /// Parameter car requires a CarViewModel argument.
        /// </param>
        /// <returns>
        /// true if the car was edited
        /// false if the car is null
        public bool EditCarTable(int id, CarViewModel car)
        {
            var theCar = _db.Cars.SingleOrDefault(cs => cs.ID == id);

            if (car == null)
            {
                return false;
            }
            else
            {
                int count = 0;
                foreach (var item in car.fuelType)
                {
                    count++;
                };
                theCar.Color = car.color;
                theCar.Doors = car.doors;
                theCar.Seating = car.seating;
                theCar.Milage = car.driven;
                theCar.Cylinders = car.cylinders;
                theCar.Horsepower = car.horsepower;
                theCar.Injection = car.injection;
                theCar.CC = car.cc;
                theCar.Weight = car.weight;
                theCar.Hybrid = count > 1;
                theCar.DriveID = car.drive;
                theCar.TransmissionID = car.transmission;
                _db.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// Delets wheel of car from db
        /// </summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// true if the wheels were removed from db
        /// false if the wheels is null
        public bool DeleteWheels(int serialNum)
        {
            var wheels = (from ws in _db.WheelCars
                            where ws.CarID == serialNum
                            select ws).ToList();

            if (wheels == null)
            {
                return false;
            }
            else
            {
                foreach (var wheel in wheels)
                {
                    _db.WheelCars.Remove(wheel);
                }
                _db.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// Delets fuel types of car from db
        /// </summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// true if the fuel types were removed from db
        /// false if the fuel types is null
        public bool DeleteFuelTypes(int serialNum)
        {
            var fuelTypes = (from ft in _db.FuelTypeCars
                where ft.CarID == serialNum
                select ft).ToList();

            if (fuelTypes == null)
            {
                return false;
            }
            else
            {
                foreach (var fuel in fuelTypes)
                {
                    _db.FuelTypeCars.Remove(fuel);
                }
                _db.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// Delets drive steering of car from db
        /// </summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// true if the drive steering were removed from db
        /// false if the drive steering is null
        public bool DeleteDriveSteering(int serialNum)
        {
            var driveSteering = (from ds in _db.DriveSteeringInfoCars
                                    where ds.CarID == serialNum
                                    select ds).ToList();

            if (driveSteering == null)
            {
                return false;
            }
            else
            {
                foreach (var drive in driveSteering)
                {
                    _db.DriveSteeringInfoCars.Remove(drive);
                }
                _db.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// Delets extra features of car from db
        /// </summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// true if the extra features were removed from db
        /// false if the extra features is null
        public bool DeleteExtraFeatures(int serialNum)
        {
            var extraFeatures = (from ef in _db.ExtraFeaturesCars
                                    where ef.CarID == serialNum
                                    select ef).ToList();

            if (extraFeatures == null)
            {
                return false;
            }
            else
            {
                foreach (var feature in extraFeatures)
                {
                    _db.ExtraFeaturesCars.Remove(feature);
                }
                _db.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// Deletes a car from Elasticsearch
        /// </summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// true if the car were removed from Elasticsearch
        /// false car was not removed
        public async Task<bool> deleteFromElasticsearch(int serialNum)
        {
            var response = await _esClientProvider.Client.DeleteByQueryAsync<CarDetailElasticDTO>(x => x
                    .Query( q => q.QueryString(qs => qs.Query("iD:"+ serialNum))
                    ));
            if (response.IsValid)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Creates a car in Elasticsearch
        /// </summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// reponse of the Elasticsearch request
        public async Task<bool> createInElasticsearch(CarDetailElasticDTO car)
        {
            var carRequestModel = new Nest.IndexRequest<CarDetailElasticDTO>(car);
            var res = await _esClientProvider.Client.IndexAsync(carRequestModel);
            return res.IsValid;
        }

        /// <summary>
        /// Finds a car in Elasticsearch by sort and search term
        /// </summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// List of cars that match the search term and condition
        public async Task<List<CarDetailElasticDTO>> findCarsInElasticsearch(string sort, string term = null)
        {
            var res = await _esClientProvider.Client.SearchAsync<CarDetailElasticDTO>(x => x
            .Query( q => q.QueryString(qs => qs.Query(term)))
            .Sort(s => s
            .Descending(sort)
            ).Take(10000));

            List<CarDetailElasticDTO> cars = new List<CarDetailElasticDTO>();
            cars = (dynamic)res.Documents;

            return cars;
        }

        /// <summary>
        /// Deletes a blob cor a car from Blob storage
        /// </summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// Response of the request
        public async Task<bool> deleteBlob(string serialNum)
        {
            var config = _configuration.GetSection("Blob").Get<List<string>>();
            var storageCredentials = new StorageCredentials(config[0],config[1]);
            var cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
            var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
            var container = cloudBlobClient.GetContainerReference(serialNum);
            var response = await container.DeleteIfExistsAsync();
            return response;
        }

        /// <summary>
        /// Creates a blob in Blob storage
        /// </summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        public async Task createBlob(string serialNum, IFormFile image)
        {
            Stream stream = image.OpenReadStream();
            var config = _configuration.GetSection("Blob").Get<List<string>>();
            var storageCredentials = new StorageCredentials(config[0],config[1]);
            var cloudStorageAccount = new CloudStorageAccount(storageCredentials, true);
            var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
            var container = cloudBlobClient.GetContainerReference(serialNum.ToString());
            await container.CreateIfNotExistsAsync();
            BlobContainerPermissions permissions = await container.GetPermissionsAsync();
            permissions.PublicAccess = BlobContainerPublicAccessType.Container;
            await container.SetPermissionsAsync(permissions);

            var newBlob = container.GetBlockBlobReference(image.FileName);
            await newBlob.UploadFromStreamAsync(stream);
        }
    }
}