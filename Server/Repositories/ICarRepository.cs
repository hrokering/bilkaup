using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bilkaup.Models.DTOModels;
using Bilkaup.Models.EntityModels;
using Bilkaup.Models.ViewModels;
using Microsoft.AspNetCore.Http;

namespace Bilkaup.Repositories
{
    /// <summary>
    /// Connects everything that has to do with cars to the database.
    /// </summary>
    public interface ICarRepository
    {
        /// <summary>
        /// Gets all unsold cars from the database.
        /// </summary>
        IEnumerable<CarCardDTO> GetCars();
        
        /// <summary>
        /// Gets informations about a car
        /// </summary>
        GetCarInfoDTO GetCarInfo();
        
        /// <summary>
        /// Adds a car to db
        /// </summary>
        int AddCar(Car car);

        /// <summary>
        /// Adds a manufacturor into db
        /// </summary>
        int AddManufacturer(Manufacturer manufacturer);

        /// <summary>
        /// Gets ID of a manufacturor by its name
        /// </summary>
        int GetManufacturerIdByName(string manufacturer);

        /// <summary>
        /// Gets ID of model by its name
        /// </summary>
        int GetModelIdByName(int manufacturerId, string model);

        /// <summary>
        /// Adds a model to db
        /// </summary>
        int AddModel(Model model);

        /// <summary>
        /// Gets all details about a car
        /// </summary>
        CarDetailDTO GetCarDetail(int carID, int serialNum);

        /// <summary>
        /// Gets informations about a car to edit
        /// </summary>
        CarViewModel GetCarForEdit(int carID);

        /// <summary>
        /// Adds informations about seller into db
        /// </summary>
        int AddSellerInfo(SaleInfo newInfo);
        
        /// <summary>
        /// Gets filters for search from db
        /// </summary>
        FilterDTO GetFilters();

        /// <summary>
        /// Gets ID of a car by serial number
        /// </summary>
        int GetCarIDBySerial(int serialNum);

        /// <summary>
        /// Marks car as sold with date
        /// </summary>
        bool SellCar(SaleInfo sold);
        
        /// <summary>
        /// Adds wheel of a car to db
        /// </summary>
        bool AddWheelCar(WheelCar wc);

        /// <summary>
        /// Adds fuel type of a car to db
        /// </summary>
        bool AddFuelTypeCar(FuelTypeCar fc);

        /// <summary>
        /// Adds drive steering informations of a car to db
        /// </summary>
        bool AddDriveSteeringInfoCar(DriveSteeringInfoCar dc);

        /// <summary>
        /// Adds extra feature of a car to db
        /// </summary>
        bool AddExtraFeaturesCars(ExtraFeaturesCar ec);
        
        /// <summary>
        /// Adds extra feature to db
        /// </summary>
        int AddExtraFeature(ExtraFeature addExtra);
        
        /// <summary>
        /// Gets all licence numbers from database
        /// </summary>
        IEnumerable<string> GetLicenceNumbers();

        /// <summary>
        /// Gets the ID of a car by it's licence number
        /// <summary>
        CarViewModel GetCarForEditExisting(int serialNum, int carId);

        /// <summary>
        /// Gets the ID of a car by it's licence number
        /// <summary>
        int GetIDByLicenceNum(string licenceNum);

        /// <summary>
        /// Adds image to Blob storate
        /// </summary>
        void addImage(string image, int serialNum, bool primaryStatus);

        /// <summary>
        /// Edits a car in db
        /// </summary>
        bool EditCarTable(int id, CarViewModel editCar);

        /// <summary>
        /// Delets wheel of car from db
        /// </summary>
        bool DeleteWheels(int carID);

        /// <summary>
        /// Delets fuel type of car from db
        /// </summary>
        bool DeleteFuelTypes(int carID);

        /// <summary>
        /// Delets drive steering of car from db
        /// </summary>
        bool DeleteDriveSteering(int carID);

        /// <summary>
        /// Delets extra feature of car from db
        /// </summary>
        bool DeleteExtraFeatures(int carID);

        /// <summary>
        /// Edits seller informations of a car in db
        /// </summary>
        AddCarResultDTO EditSellerTable(CarViewModel car, int serialNum);

        /// <summary>
        /// Deletes a car from Elasticsearch
        /// </summary>
        Task<bool> deleteFromElasticsearch(int serialNum);

        /// <summary>
        /// Finds a car in Elasticsearch by sort and search term
        /// </summary>
        Task<List<CarDetailElasticDTO>> findCarsInElasticsearch(string sort, string term = null);
        
        /// <summary>
        /// Deletes a blob cor a car from Blob storage
        /// </summary>
        Task<bool> deleteBlob(string serialNum);

        /// <summary>
        /// Creates a blob in Blob storage
        /// </summary>
        Task createBlob(string serialNum, IFormFile image);

        /// <summary>
        /// Creates a car in Elasticsearch
        /// </summary>
        Task<bool> createInElasticsearch(CarDetailElasticDTO car);
    }
}