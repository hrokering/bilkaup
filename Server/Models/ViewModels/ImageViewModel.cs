using Microsoft.AspNetCore.Http;

namespace Bilkaup.Models.ViewModels
{
    public class ImageViewModel
    {
        public IFormFile image { get; set; }
        public bool primary { get; set; }
    }
}