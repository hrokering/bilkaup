using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Bilkaup.Models.ViewModels
{
    public class ExtraFeatureViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
    } 
}