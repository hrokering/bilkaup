using System;
using System.Collections.Generic;

namespace Bilkaup.Models.DTOModels
{
    public class FilterDTO
    {
        public List<ManufacturerFilterDTO> manufacturers { get; set; }
        public List<FeatureFilterDTO> extraFeatures { get; set; }
        public List<FeatureFilterDTO> fuelTypes { get; set; }
    }
}