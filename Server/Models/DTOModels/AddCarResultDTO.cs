using System;

namespace Bilkaup.Models.DTOModels
{
    public class AddCarResultDTO
    {
        public int carId { get; set; }
        public int carSerialNum { get; set; }
    }
}