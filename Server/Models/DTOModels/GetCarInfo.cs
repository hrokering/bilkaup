using System;
using System.Collections.Generic;

namespace Bilkaup.Models.DTOModels
{
    public class GetCarInfoDTO
    {
        public IEnumerable<FuelTypeDTO> fuelTypes { get; set; }
        public IEnumerable<WheelDTO> wheels { get; set; }
        public IEnumerable<DriveSteeringDTO> driveSteering { get; set; }
        public IEnumerable<ExtraFeaturesDTO> extraFeatures { get; set; }
    }
}