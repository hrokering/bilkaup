namespace Bilkaup.Models.DTOModels
{    
    public class FeatureFilterDTO 
    {
        public string name { get; set; }
        public bool selected { get; set; }
    }
}