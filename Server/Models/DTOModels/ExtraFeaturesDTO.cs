using System;
using System.Collections.Generic;

namespace Bilkaup.Models.DTOModels
{
    public class ExtraFeaturesDTO
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}