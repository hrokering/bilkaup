﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Bilkaup.Models.DTOModels;
using Bilkaup.Models.EntityModels;
using Bilkaup.Models.ViewModels;
using Bilkaup.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Bilkaup.Services
{
    public class CarService : ICarService
    {
        private readonly ICarRepository _repo;
        
        public CarService(ICarRepository repo)
        {
            _repo = repo;
        }

        /// <summary>
        /// Counts elements in an array
        /// <summary>
        /// <param name=”array”>
        /// Parameter array requires a IEnumerable of ints argument.
        /// </param>
        /// <returns>
        /// The count of items in the array
        /// </returns>
        private int Count(IEnumerable<int> array)
        {
            int count = 0;
            foreach (var item in array)
            {
                count++;
            }

            return count;
        }

        /// <summary>
        /// Sends a car to be added to repository
        /// <summary>
        /// <param name=”car”>
        /// Parameter car requires a CarViewModel argument.
        /// </param>
        /// <returns>
        /// AddCarResultDTO with carID and carSerialNum
        /// </returns>
        public AddCarResultDTO AddCar(CarViewModel car)
        {
            var manufacturerId = CheckManufacturerByName(car.manufacturer);
            var modelId = CheckModelByName(manufacturerId, car.model);
            
            //Gets the count of fuel types to know if the car is Hybrid or not
            int fuelTypeCount = Count(car.fuelType);

            //Create the entity model of the car
            Car newCar = new Car()
            {
                LicenceNumber = car.regNum,
                ManufacturerID = manufacturerId,
                ModelID = modelId,
                Year = car.year,
                CO2 = car.co2,
                Color = car.color,
                Status = car.status,
                Doors = car.doors,
                Seating = car.seating,
                Milage = car.driven,
                Cylinders = car.cylinders,
                Horsepower = car.horsepower,
                Injection = car.injection,
                CC = car.cc,
                Weight = car.weight,
                Hybrid = fuelTypeCount > 1,
                DriveID = car.drive,
                TransmissionID = car.transmission,
                NextCheckup = car.nextCheckUp
            };

            var carID = _repo.AddCar(newCar);
            DateTime localDate = DateTime.Now;

            //Create the SaleInfo entity model
            SaleInfo newInfo = new SaleInfo()
            {
                CarSaleID = car.carSaleId,
                CarID = carID,
                DateOnSale = localDate,
                DateOfUpdate = localDate,
                SellerID = 0,
                Price = car.price,
                OnSite = car.onSite
            };

            var serialNumber = _repo.AddSellerInfo(newInfo);

            if (car.wheel != null)
            {
                foreach (var wheelId in car.wheel)
                {
                    WheelCar wc = new WheelCar()
                    {
                        CarID = serialNumber,
                        WheelID = wheelId,
                        Quantity = 4
                    };

                    _repo.AddWheelCar(wc);
                }
            }

            if (car.fuelType != null)
            {
                foreach (var fuelId in car.fuelType)
                {
                    FuelTypeCar fc = new FuelTypeCar()
                    {
                        CarID = serialNumber,
                        FuelTypeID = fuelId,
                    };

                    _repo.AddFuelTypeCar(fc);
                }
            }

            if (car.driveSteering != null)
            {
                foreach (var driveSteeringId in car.driveSteering)
                {
                    DriveSteeringInfoCar dc = new DriveSteeringInfoCar()
                    {
                        CarID = serialNumber,
                        DriveSteeringID = driveSteeringId,
                    };

                    _repo.AddDriveSteeringInfoCar(dc);
                }
            }

            if (car.extraFeatures != null)
            {
                foreach (var extraFeature in car.extraFeatures)
                {
                    ExtraFeaturesCar ec = new ExtraFeaturesCar();
                    // if the extra feature does not exist in the database add it!
                    if (extraFeature.id == 0)
                    {
                        var featureID = this.AddExtraFeature(extraFeature.name);

                        ec.CarID = serialNumber;
                        ec.ExtraFeaturesID = featureID;
                    }
                    else
                    {
                        ec.CarID = serialNumber;
                        ec.ExtraFeaturesID = extraFeature.id;
                    }

                    _repo.AddExtraFeaturesCars(ec);
                }
            }

            var result = new AddCarResultDTO()
            {
                carId = carID,
                carSerialNum = serialNumber
            };

            return result;
        }

        /// <summary>
        /// Gets a car by its licence number from repository
        /// <summary>
        /// <param name=”licenceNum”>
        /// Parameter licenceNum requires a string argument.
        /// </param>
        /// <returns>
        /// CarViewModel with car that was fetched
        /// </returns>
        public CarViewModel GetCarByLicenceNum(string licenceNum)
        {
            var id = _repo.GetIDByLicenceNum(licenceNum);

            return _repo.GetCarForEdit(id);
        }

        /// <summary>
        /// Gets a car from repo and sets the information into a CarDetailElasticDTO
        /// <summary>
        /// <param name=”car”>
        /// Parameter car requires a CarDetailDTO argument.
        /// </param>
        /// <returns>
        /// CarDetailElasticDTO with car that should be created in Elasticsearch
        /// </returns>
        public CarDetailElasticDTO getCarForElasticsearch(CarDetailDTO car)
        {            
            var year = Int32.Parse(car.year.Split('.')[2]);
            var elasticsearchCar = new CarDetailElasticDTO()
            {
                ID = car.serialNumber,
                manufacturer = car.manufacturer,
                model = car.model,
                modelType = car.modelType,
                co2 = car.co2,
                color = car.color,
                weight = car.weight,
                year = year,
                milage = car.milage,
                nextCheckup = car.nextCheckup,
                fuelTypes = car.fuelTypes,
                cylinders = car.cylinders,
                cc = car.cc,
                injection = car.injection,
                horsepower = car.horsepower,
                price = car.price,
                engineWeight = car.engineWeight,
                transmission = car.transmission,
                drive = car.drive,
                driveSteering = car.driveSteering,
                seating = car.seating,
                doors =car.doors,
                moreInfo = car.moreInfo,
                status = car.status,
                dateSale = car.dateSale.ToString(@"MM\/dd\/yyyy HH:mm"),
                dateUpdate = car.dateUpdate.ToString(@"MM\/dd\/yyyy HH:mm"),
                extraFeatures = car.extraFeatures,
                carsaleId = car.carsale.ID,
                carsaleName = car.carsale.name,
                carsaleEmail = car.carsale.email,
                carsalePhoneNum = car.carsale.phoneNum,
                carsaleAddress = car.carsale.address,
                carsaleWebpage = car.carsale.webpage,
                primaryPhotoUrl = car.primaryPhotoUrl,
                photosUrl  =  car.photosUrl,
                hybrid = car.hybrid,
                wheels = car.wheels,
                onSite = car.onSite,
                licenceNumber = car.licenceNumber
            };
            return elasticsearchCar;
        }

        /// <summary>
        /// Sends extra feature to repository
        /// <summary>
        /// <param name=”addExtra”>
        /// Parameter addExtra requires a string argument.
        /// </param>
        /// <returns>
        /// True if the extra feature was created in repository
        /// False if the extra feature was not created in repository
        /// </returns>
        public int AddExtraFeature(string addExtra)
        {
            ExtraFeature add = new ExtraFeature();
            
            add.Name = addExtra;

            return _repo.AddExtraFeature(add);
        }

        /// <summary>
        /// Calls repository to see if the manufacuter exists
        /// <summary>
        /// <param name=”manufacturer”>
        /// Parameter manufacturer requires a string argument.
        /// </param>
        /// <returns>
        /// The ID of the manufacturor
        /// </returns>
        public int CheckManufacturerByName(string manufacturer)
        {
            var manufacturerID = _repo.GetManufacturerIdByName(manufacturer);

            // If the manufacturer doesn't exist we add him to the DB
            if (manufacturerID == 0)
            {
                Manufacturer manu = new Manufacturer()
                {
                    Name = manufacturer
                };

                manufacturerID = _repo.AddManufacturer(manu);
            }

            return manufacturerID;
        }

        /// <summary>
        /// Calls repository to see if the model exists
        /// <summary>
        /// <param name=”model”>
        /// Parameter model requires a string argument.
        /// </param>
        /// <returns>
        /// The ID of the model
        /// </returns>
        public int CheckModelByName(int manufacturerId, string model)
        {
            var modelID = _repo.GetModelIdByName(manufacturerId, model);

            // If the manufacturer doesn't exist we add him to the DB
            if (modelID == 0)
            {
                Model mod = new Model()
                {
                    Name = model,
                    ManufID = manufacturerId
                };

                modelID = _repo.AddModel(mod);
            }

            return modelID;
        }

        /// <summary>
        /// Calls repository to get a car by serial number
        /// <summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// CarDetailDTO with information about the car if the car exists
        /// null if the car does not exist
        /// </returns>
        public CarDetailDTO GetCarBySerialNum(int serialNum)
        {
            var carId = _repo.GetCarIDBySerial(serialNum);

            return _repo.GetCarDetail(carId, serialNum);
        }

        /// <summary>
        /// Gets all cars from repository
        /// <summary>
        /// <returns>
        /// IEnumerable with CarCardDTO that includes all cars
        /// </returns>
        public IEnumerable<CarCardDTO> GetCars()
        {
            var cars = _repo.GetCars();

            return cars;
        }
        
        /// <summary>
        /// Gets information about a single car from repository
        /// <summary>
        /// <returns>
        /// GetCarInfoDTO with information about the car
        /// </returns>
        public GetCarInfoDTO GetCarInfo()
        {
            return _repo.GetCarInfo();
        }
        
        /// <summary>
        /// Gets all filters from repository
        /// <summary>
        /// <returns>
        /// FilterDTO with all filters
        /// </returns>
        public FilterDTO GetFilters()
        {
            return _repo.GetFilters();
        }

        /// <summary>
        /// Calls repository with the car to be sold
        /// <summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// true - if the car was changed correctly
        /// false - if the car does not exist
        public bool SellCar(int serialNum)
        {
            DateTime localDate = DateTime.Now;

            SaleInfo soldCar = new SaleInfo()
            {
                DateOfSale = localDate,
                SerialNum = serialNum
            };

            return _repo.SellCar(soldCar);
        }

        /// <summary>
        /// Gets all licence numbers from repository
        /// <summary>
        /// <returns>
        /// List of all licence numbers
        public IEnumerable<string> GetLicenceNumbers()
        {
            return _repo.GetLicenceNumbers();
        }

        /// <summary>
        /// Gets a car for edit by its serial number
        /// <summary>
        /// <param name=”licenceNum”>
        /// Parameter licenceNum requires an int argument.
        /// </param>
        /// <returns>
        /// CarViewModel with the car that should be edited
        /// NULL - if the car does not exist
        public CarViewModel GetCarBySerialNumEdit(int licenceNum)
        {
            var id = _repo.GetCarIDBySerial(licenceNum);

            return _repo.GetCarForEditExisting(licenceNum, id);
        }
        
        /// <summary>
        /// Sends image to repository with its information
        /// <summary>
        /// <param name=”image”>
        /// Parameter image requires an string argument.
        /// </param>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <param name=”primaryStatus”>
        /// Parameter primaryStatus requires an bool argument.
        /// </param>
        public void addImage(string image, int serialNum, bool primaryStatus)
        {
            _repo.addImage(image, serialNum, primaryStatus);
        }

        /// <summary>
        /// Sends correct information about a car to repository
        /// <summary>
        /// <param name=”car”>
        /// Parameter car requires a string argument.
        /// </param>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// AddCarResultDTO with seller information
        public AddCarResultDTO EditCar(CarViewModel car, int serialNum) 
        {
            var manufacturerId = CheckManufacturerByName(car.manufacturer);
            var modelId = CheckModelByName(manufacturerId, car.model);
            
            int fuelTypeCount = Count(car.fuelType);

            Car newCar = new Car()
            {
                LicenceNumber = car.regNum,
                ManufacturerID = manufacturerId,
                ModelID = modelId,
                Year = car.year,
                CO2 = car.co2,
                Color = car.color,
                Status = car.status,
                Doors = car.doors,
                Seating = car.seating,
                Milage = car.driven,
                Cylinders = car.cylinders,
                Horsepower = car.horsepower,
                Injection = car.injection,
                CC = car.cc,
                Weight = car.weight,
                Hybrid = fuelTypeCount > 1,
                DriveID = car.drive,
                TransmissionID = car.transmission,
                NextCheckup = car.nextCheckUp
            };

            var check = _repo.EditCarTable(car.id, car);

            var sellerCheck = _repo.EditSellerTable(car, serialNum);


            if (car.wheel != null)
            {
                var deleted = _repo.DeleteWheels(serialNum);

                foreach (var wheelId in car.wheel)
                {
                    WheelCar wc = new WheelCar()
                    {
                        CarID = serialNum,
                        WheelID = wheelId,
                        Quantity = 4
                    };

                    _repo.AddWheelCar(wc);
                }
            }

            if (car.fuelType != null)
            {
                var deleted = _repo.DeleteFuelTypes(serialNum);

                foreach (var fuelId in car.fuelType)
                {
                    FuelTypeCar fc = new FuelTypeCar()
                    {
                        CarID = serialNum,
                        FuelTypeID = fuelId,
                    };

                    _repo.AddFuelTypeCar(fc);
                }
            }

            if (car.driveSteering != null)
            {
                var deleted = _repo.DeleteDriveSteering(serialNum);

                foreach (var driveSteeringId in car.driveSteering)
                {
                    DriveSteeringInfoCar dc = new DriveSteeringInfoCar()
                    {
                        CarID = serialNum,
                        DriveSteeringID = driveSteeringId,
                    };

                    _repo.AddDriveSteeringInfoCar(dc);
                }
            }

            if (car.extraFeatures != null)
            {
                var deleted = _repo.DeleteExtraFeatures(serialNum);

                foreach (var extraFeature in car.extraFeatures)
                {
                    ExtraFeaturesCar ec = new ExtraFeaturesCar();
                    // if the extra feature does not exist in the database add it!
                    if (extraFeature.id == 0)
                    {
                        var featureID = this.AddExtraFeature(extraFeature.name);

                        ec.CarID = serialNum;
                        ec.ExtraFeaturesID = featureID;
                    }
                    else
                    {
                        ec.CarID = serialNum;
                        ec.ExtraFeaturesID = extraFeature.id;
                    }

                    _repo.AddExtraFeaturesCars(ec);
                }
            }
            

            return sellerCheck;
        
        }

        /// <summary>
        /// Sends correct information about a car to repository
        /// <summary>
        /// <param name=”car”>
        /// Parameter car requires a string argument.
        /// </param>
        /// <returns>
        /// Serial Number of the sale
        public int AddExistingCar(CarViewModel car)
        {
            var manufacturerId = CheckManufacturerByName(car.manufacturer);
            var modelId = CheckModelByName(manufacturerId, car.model);
            
            int fuelTypeCount = Count(car.fuelType);

            var check = _repo.EditCarTable(car.id, car);

            int serialNum = 0;
            if (check)
            {
                DateTime localDate = DateTime.Now;

                SaleInfo newInfo = new SaleInfo()
                {
                    CarSaleID = car.carSaleId,
                    CarID = car.id,
                    DateOnSale = localDate,
                    DateOfUpdate = localDate,
                    SellerID = 0,
                    Price = car.price,
                    OnSite = car.onSite
                };

                serialNum = _repo.AddSellerInfo(newInfo);
            }

            if (car.wheel != null)
            {
                foreach (var wheelId in car.wheel)
                {
                    WheelCar wc = new WheelCar()
                    {
                        CarID = serialNum,
                        WheelID = wheelId,
                        Quantity = 4
                    };

                    _repo.AddWheelCar(wc);
                }
            }

            if (car.fuelType != null)
            {
                foreach (var fuelId in car.fuelType)
                {
                    FuelTypeCar fc = new FuelTypeCar()
                    {
                        CarID = serialNum,
                        FuelTypeID = fuelId,
                    };

                    _repo.AddFuelTypeCar(fc);
                }
            }

            if (car.driveSteering != null)
            {
                foreach (var driveSteeringId in car.driveSteering)
                {
                    DriveSteeringInfoCar dc = new DriveSteeringInfoCar()
                    {
                        CarID = serialNum,
                        DriveSteeringID = driveSteeringId,
                    };

                    _repo.AddDriveSteeringInfoCar(dc);
                }
            }

            if (car.extraFeatures != null)
            {
                foreach (var extraFeature in car.extraFeatures)
                {
                    ExtraFeaturesCar ec = new ExtraFeaturesCar();
                    // if the extra feature does not exist in the database add it!
                    if (extraFeature.id == 0)
                    {
                        var featureID = this.AddExtraFeature(extraFeature.name);

                        ec.CarID = serialNum;
                        ec.ExtraFeaturesID = featureID;
                    }
                    else
                    {
                        ec.CarID = serialNum;
                        ec.ExtraFeaturesID = extraFeature.id;
                    }

                    _repo.AddExtraFeaturesCars(ec);
                }
            }

            return serialNum;
        }
        
        /// <summary>
        /// Sends a serial number of a sale to be deleted in Elasticsearch
        /// <summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <returns>
        /// Response from repository
        public Task<bool> deleteFromElasticsearch(int serialNum)
        {
            return _repo.deleteFromElasticsearch(serialNum);
        }

        /// <summary>
        /// Sends sort string and term string to repository to be searched in Elasticsearch
        /// <summary>
        /// <param name=”sort”>
        /// Parameter sirt requires a string argument.
        /// </param>
        /// <param name=”term”>
        /// Parameter term requires a string argument.
        /// </param>
        /// <returns>
        /// Response from repository
        public Task<List<CarDetailElasticDTO>> findCarsInElasticsearch(string sort, string term = null)
        {
            return _repo.findCarsInElasticsearch(sort, term);
        }

        /// <summary>
        /// Sends serial number to repository to delete from Blob Storage
        /// <summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires a string argument.
        /// </param>
        /// <returns>
        /// Response from repository
        public Task<bool> deleteBlob(string serialNum)
        {
            return _repo.deleteBlob(serialNum);
        }

        /// <summary>
        /// Sends an image file and serial number to repository
        /// <summary>
        /// <param name=”serialNum”>
        /// Parameter serialNum requires an int argument.
        /// </param>
        /// <param name=”image”>
        /// Parameter image requires an IFormFile argument.
        /// </param>
        /// <returns>
        /// Response from repository
        public Task createBlob(string serialNum, IFormFile image)
        {
            return _repo.createBlob(serialNum, image);
        }

        /// <summary>
        /// Sends a car to repository to be created in Elasticsearch
        /// <summary>
        /// <param name=”car”>
        /// Parameter car requires a CarDetailElasticDTO argument.
        /// </param>
        /// <returns>
        /// Response from repository
        public Task<bool> createInElasticsearch(CarDetailElasticDTO car)
        {
            return _repo.createInElasticsearch(car);
        }
    }
}