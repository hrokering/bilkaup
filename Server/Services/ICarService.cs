using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bilkaup.Models.DTOModels;
using Bilkaup.Models.EntityModels;
using Bilkaup.Models.ViewModels;
using Microsoft.AspNetCore.Http;

namespace Bilkaup.Services
{
    public interface ICarService
    {
        /// <summary>
        /// Gets all cars from repository
        /// <summary>
        IEnumerable<CarCardDTO> GetCars();        

        /// <summary>
        /// Sends a car to be added to repository
        /// <summary>
        AddCarResultDTO AddCar(CarViewModel car);

        /// <summary>
        /// Sends correct information about a car to be edited to repository
        /// <summary>
        AddCarResultDTO EditCar(CarViewModel car, int serialNum);

        /// <summary>
        /// Calls repository to see if the manufacuter exists
        /// <summary>
        int CheckManufacturerByName(string manufacturer);

        /// <summary>
        /// Calls repository to see if the model exists
        /// <summary>
        int CheckModelByName(int manufacturerId, string model);
        
        /// <summary>
        /// Gets all filters from repository
        /// <summary>
        FilterDTO GetFilters();
        
        /// <summary>
        /// Gets information about a single car
        /// <summary>
        GetCarInfoDTO GetCarInfo();

        /// <summary>
        /// Calls repository to get a car by serial number
        /// <summary>
        CarDetailDTO GetCarBySerialNum(int id);

        /// <summary>
        /// Calls repository with the car to be sold
        /// <summary>
        bool SellCar(int serialNum);

        /// <summary>
        /// Gets all licence numbers from repository
        /// <summary>
        IEnumerable<string> GetLicenceNumbers();

        /// <summary>
        /// Gets a car for edit by its serial number
        /// <summary>
        CarViewModel GetCarBySerialNumEdit(int licenceNum);

        /// <summary>
        /// Sends extra feature to repository
        /// <summary>
        int AddExtraFeature(string addExtra);

        /// <summary>
        /// Sends image to repository with its information
        /// <summary>
        void addImage(string image, int serialNum, bool primaryStatus);

        /// <summary>
        /// Gets a car from repo and sets the information into a CarDetailElasticDTO
        /// <summary>
        CarDetailElasticDTO getCarForElasticsearch(CarDetailDTO car);

        /// <summary>
        /// Gets a car by its licence number from repository
        /// <summary>
        CarViewModel GetCarByLicenceNum(string licenceNum);

        /// <summary>
        /// Sends correct information about a car to repository
        /// <summary>
        int AddExistingCar(CarViewModel car);

        /// <summary>
        /// Sends a serial number of a sale to be deleted in Elasticsearch
        /// <summary>
        Task<bool> deleteFromElasticsearch(int serialNum);

        /// <summary>
        /// Sends sort string and term string to repository to be searched in Elasticsearch
        /// <summary>
        Task<List<CarDetailElasticDTO>> findCarsInElasticsearch(string sort, string term = null);
        
        /// <summary>
        /// Sends serial number to repository to delete from Blob Storage
        /// <summary>
        Task<bool> deleteBlob(string serialNum);

        /// <summary>
        /// Sends an image file and serial number to repository
        /// <summary>
        Task createBlob(string serialNum, IFormFile image);

        /// <summary>
        /// Sends a car to repository to be created in Elasticsearch
        /// <summary>
        Task<bool> createInElasticsearch(CarDetailElasticDTO car);
    }
}