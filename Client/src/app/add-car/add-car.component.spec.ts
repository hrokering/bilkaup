import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarService } from '../car.service';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CarViewModel, CarDTO, WheelDTO, FuelTypeDTO, DriveSteeringDTO, ExtraFeaturesDTO, ExtraFeatureViewModel } from '../app.models';
import { FileUploadModule } from 'primeng/fileupload';
import { MenuItem } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { TabViewModule } from 'primeng/tabview';
import { AddCarComponent } from './add-car.component';
import { ComponentFixture, TestBed, async, fakeAsync  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ConnectionBackend, RequestOptions, BaseRequestOptions, Http } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { AuthService } from '../AuthenticationServices/auth.service';
import { MockAuthService } from '../MockServices/mock-auth.service';

describe('AddCarComponent', () => {
  let component: AddCarComponent;
  let fixture: ComponentFixture<AddCarComponent>;

  const mockToastService = {
    error: jasmine.createSpy('error'),
    warning: jasmine.createSpy('warning'),
    success: jasmine.createSpy('success'),
    info: jasmine.createSpy('info')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        FileUploadModule,
        TabViewModule
      ],
      declarations: [ AddCarComponent ],
      providers: [{
        provide: ConnectionBackend,
        useClass: MockBackend
      },
      {
        provide: AuthService,
        useClass: MockAuthService },
      {
        provide: RequestOptions,
        useClass: BaseRequestOptions
      },
      Http,
      CarService,
      {
        provide: ToastrService,
        useValue: mockToastService
      }
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have form defined', () => {
    expect(component.myForm).toBeDefined();
  });

  it('should have called function submit()', () => {
    spyOn(component, 'submit').and.callThrough();
    component.car.co2 = 0;
    component.submit();
    expect(component.submit).toHaveBeenCalled();
    component.carReady = true;
    const hostElement = fixture.nativeElement;
    component.submit();
    expect(component.submit).toHaveBeenCalled();
  });

  it('should have called function addCar()', () => {
    spyOn(component, 'addCar').and.callThrough();
    component.addCar(null);
    expect(component.addCar).toHaveBeenCalled();
    this.filesToUpload = ['', ''];
    component.addCar(null);
    expect(component.addCar).toHaveBeenCalled();
  });

  it('should have called function getCarFromSamgongustofa()', () => {
    spyOn(component, 'getCarFromSamgongustofa').and.callThrough();
    component.regNum = 'LMS68';
    component.getCarFromSamgongustofa();
    expect(component.getCarFromSamgongustofa).toHaveBeenCalled();
  });

  it('should have called function setCarInfo()', () => {
    spyOn(component, 'setCarInfo').and.callThrough();
    const info = {
        wheels: [''],
        fuelTypes: [''],
        driveSteeringInfos: [''],
        extraFeatures: new Array<ExtraFeaturesDTO>(
          {id: 1, name: 'Hiti í sætum'},
          {id: 2, name: 'Rafdrifnar rúður'})
      };

    component.setCarInfo(info);
    expect(component.setCarInfo).toHaveBeenCalled();
    expect(component.extraFeatures[0]).toEqual(info.extraFeatures[0]);
  });

  it('should have called function setCarFromSamgongustofaInfo()', () => {
    spyOn(component, 'setCarFromSamgongustofaInfo').and.callThrough();
    const car = {
      fastanumer: '', tegund: '', firstskrad: '', undirtegund: '',
      litur: '', co2losun: '', eiginthyngd: '', stada: '', naestaadalskodun: ''};
    component.setCarFromSamgongustofaInfo(car);
    expect(component.setCarFromSamgongustofaInfo).toHaveBeenCalled();
  });

  it('should have called function addFeature()', () => {
    spyOn(component, 'addFeature').and.callThrough();
    component.extraFeatures = [{id: 1, name: 'feature'}, {id: 1, name: 'bla'}];
    component.addFeature('hiti í sætum');
    expect(component.addFeature).toHaveBeenCalled();
    component.addFeature('feature');
    expect(component.addFeature).toHaveBeenCalled();
  });

  it('should have called function myUploader()', () => {
    spyOn(component, 'myUploader').and.callThrough();
    const event = {
      files: [{
          name: 'bla'
      }]
    };
    component.uploadedFiles = [];
    component.myUploader(event);
    expect(component.myUploader).toHaveBeenCalled();
  });

  it('should have called function searchLicence() with none existing car', () => {
    spyOn(component, 'searchLicence').and.callThrough();
    component.regNum = 'LMS68';
    component.carExists = true;
    component.licenceNumbers = ['LMS68'];
    const event = {
      target: {
        value: 'bla'
      }
    };
    component.searchLicence(event);
    expect(component.searchLicence).toHaveBeenCalled();
  });

  it('should have called function searchLicence() with existing car', () => {
    spyOn(component, 'searchLicence').and.callThrough();
    component.regNum = 'LMS68';
    component.carExists = true;
    const event = {
      target: {
        value: 'bla'
      }
    };
    component.searchLicence(event);
    expect(component.searchLicence).toHaveBeenCalled();
  });

  it('should have called function fuelTypeSelected() with fuel type', () => {
    spyOn(component, 'fuelTypeSelected').and.callThrough();
    component.car = new CarViewModel();
    component.car.fuelType = [1];
    component.fuelTypeSelected(1);
    expect(component.fuelTypeSelected).toHaveBeenCalled();
  });

  it('should have called function fuelTypeSelected() without fuel type', () => {
    spyOn(component, 'fuelTypeSelected').and.callThrough();
    component.car = new CarViewModel();
    component.fuelTypeSelected(1);
    expect(component.fuelTypeSelected).toHaveBeenCalled();
  });

  it('should have called function wheelsSelected() with wheels', () => {
    spyOn(component, 'wheelsSelected').and.callThrough();
    component.car = new CarViewModel();
    component.car.wheel = [1];
    component.wheelsSelected(1);
    expect(component.wheelsSelected).toHaveBeenCalled();
  });

  it('should have called function wheelsSelected() without weels', () => {
    spyOn(component, 'wheelsSelected').and.callThrough();
    component.car = new CarViewModel();
    component.wheelsSelected(1);
    expect(component.wheelsSelected).toHaveBeenCalled();
  });

  it('should have called function driveSteeringSelected() with driveSteering', () => {
    spyOn(component, 'driveSteeringSelected').and.callThrough();
    component.car = new CarViewModel();
    component.car.driveSteering = [1];
    component.driveSteeringSelected(1);
    expect(component.driveSteeringSelected).toHaveBeenCalled();
  });

  it('should have called function driveSteeringSelected() without driveSteering', () => {
    spyOn(component, 'driveSteeringSelected').and.callThrough();
    component.car = new CarViewModel();
    component.driveSteeringSelected(1);
    expect(component.driveSteeringSelected).toHaveBeenCalled();
  });

  it('should have called function removeFeature()', () => {
    spyOn(component, 'removeFeature').and.callThrough();
    component.selectedFeatures = [{name: 'bla', id: 1}];
    component.removeFeature('bla');
    expect(component.removeFeature).toHaveBeenCalled();
  });

});
