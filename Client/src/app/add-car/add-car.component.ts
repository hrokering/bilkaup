import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarService } from '../car.service';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { CarViewModel, CarDTO, WheelDTO, FuelTypeDTO, DriveSteeringDTO, ExtraFeaturesDTO, ExtraFeatureViewModel } from '../app.models';
import { FileUploadModule } from 'primeng/fileupload';
import { MenuItem } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';
import { TabViewModule } from 'primeng/tabview';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.css']
})

export class AddCarComponent implements OnInit {

  public serialNum: number;
  private sub: any;
  myForm: FormGroup;
  carChange: CarViewModel;
  car: CarViewModel;
  carReady: boolean;
  wheels: WheelDTO[];
  fuelTypes: FuelTypeDTO[];
  driveSteeringInfos: DriveSteeringDTO[];
  licenceNumbers: string[];
  regNum: string;
  carExists: boolean;
  extraFeatures: ExtraFeaturesDTO[];
  extraFeature: string[];
  selectedFeatures: ExtraFeatureViewModel[];
  feature: boolean;
  uploadedFiles: any[] = [];
  items: MenuItem[];
  activeItem: MenuItem;
  img: FormData;
  filesToUpload: File[] = null;
  carAdded: boolean;
  primary: string;
  edit: boolean;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: CarService,
    private fb: FormBuilder,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.edit = false;
    this.carAdded = false;
    this.carReady = false;
    this.feature = false;
    this.extraFeature = new Array<string>();
    this.selectedFeatures = new Array<ExtraFeatureViewModel>();
    this.licenceNumbers = new Array<string>();
    this.car = new CarViewModel();
    this.img = new FormData();

    this.sub = this.route.params.subscribe(
      params => {
        // get the serial number from params to see if we are in edit. If no serial number, we are adding new car
        if (params['serialNum']) {
          this.serialNum = +params['serialNum'];
          this.service.getCarBySerialNumEdit(this.serialNum, parseInt(localStorage.getItem('id'), null)).subscribe(
          currentCar => {
            this.car = currentCar;
            // Add the extra features so the user sees them
            for (const feature of this.car.extraFeatures) {
              this.addFeature(feature.name);
            }
            this.carReady = true;
            this.regNum = this.car.regNum;
            this.edit = true;
          }, err => {
            this.carReady = false;
            this.toastr.error('Tókst ekki að sækja bíl', 'Error!');
          }
        );
      }
    });

    this.service.getLicenceNumbers().subscribe(
      regNum => {
        this.licenceNumbers = regNum;
      }, err => {
        // this.toastr.error('Tókst ekki að sækja bílnúmer', 'Error!');
    });

    this.service.getCarInfo().subscribe(
      info => {
        this.setCarInfo(info);
      }, err => {
        // this.toastr.error('Tókst ekki að sækja aukahluti', 'Error!');
      });

    this.myForm = this.fb.group({
      regNum: new FormControl('', Validators.required)
    });
  }

  // Sets car information
  setCarInfo(info) {
    this.wheels = info.wheels;
    this.fuelTypes = info.fuelTypes;
    this.driveSteeringInfos = info.driveSteering;
    this.extraFeatures = info.extraFeatures;
  }

  // Submits a car
  submit() {
    if (this.carReady === false) {
      return;
    }

    const wheelInputs = document.getElementsByName('wheel');
    this.car.wheel = new Array();
    let i;
    for (i = 0; i < wheelInputs.length; i++) {
      const chk = <HTMLInputElement>wheelInputs[i];
      if (chk.checked) {
        this.car.wheel.push(+wheelInputs[i].id);
      }
    }

    const fuelInputs = document.getElementsByName('fuel');
    this.car.fuelType = new Array();

    for (i = 0; i < fuelInputs.length; i++) {
      if ((<HTMLInputElement>fuelInputs[i]).checked) {
        this.car.fuelType.push(+fuelInputs[i].id);
      }
    }

    const driveSteeringInputs = document.getElementsByName('driveSteering');
    this.car.driveSteering = new Array();

    for (i = 0; i < driveSteeringInputs.length; i++) {
      if ((<HTMLInputElement>driveSteeringInputs[i]).checked) {
        this.car.driveSteering.push(+driveSteeringInputs[i].id);
      }
    }
    this.car.extraFeatures = this.selectedFeatures;
    this.car.carSaleId = +localStorage.getItem('id');

    // If the car does not have co2, we need to give it default 0
    // Otherwise it will fail in server
    if (this.car.co2.toString() === '') {
      this.car.co2 = 0;
    }
    const reply = this.service.addCar(this.car, this.serialNum).subscribe(
      car => {
        this.addCar(car);
      }, err => {
        this.toastr.error('Tókst ekki senda bíl', 'Error!');
    });
  }

  // Adds a car do databases
  addCar(car) {
        // this.router.navigate(['../']);
        // tslint:disable-next-line:forin
        let counter = 0;
        // tslint:disable-next-line:forin
        for (const img in this.filesToUpload) {
          const repl = this.service.addImage(this.filesToUpload[img], car, this.primary).subscribe(
            bla => {
              counter += 1;
             if (counter === this.filesToUpload.length) {
              const elasticCreate = this.service.elasticCreate(car).subscribe(
                res => {
                  this.toastr.success('Bíl hefur verið bætt við', 'Aðgerð tókst!');
                  this.router.navigate(['../carsale']);
                }
              );
            }
            }
          );
        }
        if (this.filesToUpload === null) {
          const elasticCreate = this.service.elasticCreate(car).subscribe(
            res => {
              this.toastr.success('Bíl hefur verið bætt við', 'Aðgerð tókst!');
              this.router.navigate(['../carsale']);
            }
          );
        }
  }

  // Gets car information from Samgongustofa
  getCarFromSamgongustofa() {
    const reply = this.service.getCarFromSamgongustofa(this.regNum).subscribe(
      car => {
        this.setCarFromSamgongustofaInfo(car);
      }, err => {
        this.toastr.error('Tókst ekki að sækja bíl frá Samgöngustofu', 'Error!');
      }
    );
  }

  // Sets car information from Samgongustofa to the car
  setCarFromSamgongustofaInfo(car) {
    this.car.regNum = car.fastanumer;
    this.car.manufacturer = car.tegund;
    this.car.year = car.firstskrad;
    this.car.model = car.undirtegund;
    this.car.color = car.litur;
    this.car.co2 = car.co2losun;
    this.car.weight = car.eiginthyngd;
    this.car.status = car.stada;
    this.car.nextCheckUp = car.naestaadalskodun;
    this.car.seating = 5;
    this.car.doors = 5;

    this.carReady = true;
    this.toastr.success('Bíll með bílnúmerið ' + car.fastanumer + ' hefur verið sóttur', 'Aðgerð tókst!');
  }

  // Searches whether the input for licence number already exists
  searchLicence(event: any) {
    this.regNum = event.target.value.toUpperCase();
    if (this.licenceNumbers.indexOf(this.regNum) > -1 && !this.carExists) {
      this.carExists = true;
      this.service.getCarByLicenceNum(this.regNum).subscribe(
        car => {
          this.car = car;
          this.carReady = true;
          this.toastr.success('Bíllinn með fastanúmerið ' + this.regNum + ' hefur verið sóttur fyrir þig!', 'Aðgerð tókst!');
        }, err => {
          this.toastr.error('Tókst ekki sækja bíl', 'Error!');
      });
    } else {
      this.carExists = false;
      this.car = new CarViewModel();
      this.carReady = false;
    }
  }

  // Adds a feature to the car
  addFeature(feature: string) {
    if (feature !== null || feature !== '') {
      for (const f of this.extraFeatures) {
        if (feature === f.name) {
          const fea = new ExtraFeatureViewModel();
          fea.name = f.name;
          fea.id = f.id;
          if (this.extraFeature.indexOf(feature) > -1) {
            this.feature = true;
            this.toastr.info('Þessi aukabúnaður er núþegar skráður á þennan bíl', 'Athugið!');
          } else {
            this.selectedFeatures.push(fea);
            this.extraFeature.push(feature);
            this.feature = true;
          }
        }
      }
      // if this extra feature does not exist, we give it the id 0
      if (!this.feature) {
        const fea = new ExtraFeatureViewModel();
        fea.name = feature;
        fea.id = 0;
        this.selectedFeatures.push(fea);
        this.extraFeature.push(feature);
      }
    }
    this.feature = false;
  }

  // Removes a feature from the car
  removeFeature(feature: string) {
    const index = this.extraFeature.indexOf(feature);
    if (index > -1) {
      this.extraFeature.splice(index, 1);
    }

    const check = this.selectedFeatures.findIndex(fea => fea.name === feature);
    if (check > -1) {
      this.selectedFeatures.splice(check, 1);
    }
  }

  // Uploads pictures
  myUploader(event) {
    for (const file of event.files) {
      this.uploadedFiles.push(file);
    }

    this.filesToUpload = event.files;
    this.primary = this.filesToUpload[0].name;
    this.toastr.info('Myndirnar hafa verið settar inn', 'Innhölun');
  }

  // Makes a fuel type selected
  fuelTypeSelected(id) {
    if (this.car.fuelType !== undefined) {
      return this.car.fuelType.indexOf(id) >= 0;
    } else {
      return false;
    }
  }

  // Makes a wheel selected
  wheelsSelected(id) {
    if (this.car.wheel !== undefined) {
      return this.car.wheel.indexOf(id) >= 0;
    } else {
      return false;
    }
  }

  // Makes a drive steering selected
  driveSteeringSelected(id) {
    if (this.car.driveSteering !== undefined) {
      return this.car.driveSteering.indexOf(id) >= 0;
    } else {
      return false;
    }
  }
}
