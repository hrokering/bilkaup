import { Component, OnInit } from '@angular/core';
import { CarDetailDTO } from '../app.models';
import { ActivatedRoute, Router } from '@angular/router';
import { CarService } from '../car.service';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  public carReady: boolean;
  private sub: any;
  private car: CarDetailDTO;
  public serialNum: number;
  public carsaleId: number;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: CarService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.carReady = false;
    // Check if we havea carsale id to see if the user is selling this car
    this.carsaleId = parseInt(localStorage.getItem('id'), null);

    this.sub = this.route.params.subscribe(
      params => {
        // Get the serial number from params
        this.serialNum = +params['serialNum'];
        this.service.getCar(this.serialNum).subscribe(
          currentCar => {
            this.car = currentCar[0];

            if (this.car === undefined) {
              this.carReady = false;
              this.toastr.error('Bíll með þetta raðnúmer fannst ekki', 'Error!');
            } else {
              this.carReady = true;
            }
          }, err => {
            this.carReady = false;
            this.toastr.error('Bíll með þetta raðnúmer fannst ekki', 'Error!');
        });
    });
  }
  // Returns a number with thousands points
    toThousand(num) {
    if (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
    return null;
  }

  // Returns a date with correct dates and month names
  toISdate(input) {
    if (input !== undefined) {
      const date = input.split('/', 3);
        const year = date[2].split(' ', 3);
        let month;
        let day;

        // Convert months to Icelandic
        if (date[1].indexOf('01') !== -1) {
            month = 'janúar';
        } else if (date[1].indexOf('02') !== -1) {
            month = 'febrúar';
        } else if (date[1].indexOf('03') !== -1) {
            month = 'mars';
        } else if (date[1].indexOf('04') !== -1) {
            month = 'apríl';
        } else if (date[1].indexOf('05') !== -1) {
            month = 'maí';
        } else if (date[1].indexOf('06') !== -1) {
            month = 'júní';
        } else if (date[1].indexOf('07') !== -1) {
            month = 'júlí';
        } else if (date[1].indexOf('08') !== -1) {
            month = 'ágúst';
        } else if (date[1].indexOf('09') !== -1) {
            month = 'september';
        } else if (date[1].indexOf('10') !== -1) {
            month = 'október';
        } else if (date[1].indexOf('11') !== -1) {
            month = 'nóvember';
        } else if (date[1].indexOf('12') !== -1) {
            month = 'desember';
        }

        // If date starts with 0, only use second letter of the date section
        if (date[0][0] !== '0') {
            day = date[0];
        } else {
            day = date[0][1];
        }

        // Returns as d. month 20YY or dd. month 20YY
        return day + '. ' + month + ' ' + year[0];
    }
    return null;
  }

  // splits a string to get the year
  getYear(date) {
    const dateArray = date.split('.');
    return dateArray[2];
  }
}
