import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CarComponent } from './car.component';
import { Component, OnInit, Directive } from '@angular/core';
import { CarDetailDTO } from '../app.models';
import { ActivatedRoute, Router } from '@angular/router';
import { CarService } from '../car.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MockCarService, mockService } from '../MockServices/mockCar.service';
import { ConnectionBackend, RequestOptions, BaseRequestOptions, Http } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { AppModule } from '../app.module';
import { ToastrService } from 'ngx-toastr';

describe('CarComponent', () => {
  let component: CarComponent;
  let fixture: ComponentFixture<CarComponent>;

  const mockToastService = {
    error: jasmine.createSpy('error'),
    warning: jasmine.createSpy('warning'),
    success: jasmine.createSpy('success')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CarComponent
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
        ProgressSpinnerModule
      ],
      providers: [{
        provide: CarService,
        useValue: mockService
      }, {
        provide: ConnectionBackend,
        useClass: MockBackend
      },
      {
        provide: RequestOptions,
        useClass: BaseRequestOptions
      },
      {
        provide: ToastrService,
        useValue: mockToastService
      },
      Http, AppModule
    ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should get car', () => {
    component.serialNum = 1;
    expect(component).toBeTruthy();
  });

  it('should call getYear', () => {
    spyOn(component, 'getYear').and.callThrough();
    component.getYear('01.01.1990');
    expect(component.getYear).toHaveBeenCalled();
  });
  it('should call toThousand', () => {
    spyOn(component, 'toThousand').and.callThrough();
    component.toThousand('10000000000');
    expect(component.toThousand).toHaveBeenCalled();
  });
  it('should call toISdate', () => {
    spyOn(component, 'toISdate').and.callThrough();
    component.toISdate('01/01/1990');
    component.toISdate('02/02/1990');
    component.toISdate('03/03/1990');
    component.toISdate('04/04/1990');
    component.toISdate('05/05/1990');
    component.toISdate('06/06/1990');
    component.toISdate('07/07/1990');
    component.toISdate('08/08/1990');
    component.toISdate('09/09/1990');
    component.toISdate('10/10/1990');
    component.toISdate('11/11/1990');
    component.toISdate('12/12/1990');
    expect(component.toISdate).toHaveBeenCalled();
  });
});
