import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CarService } from './car.service';
import { mockService } from './MockServices/mockCar.service';
import { RoleService } from './AuthenticationServices/role.service';
import { MockRoleService } from './MockServices/mock-role.service';
import { ConnectionBackend, RequestOptions, BaseRequestOptions, Http } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { ToastrService } from 'ngx-toastr';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  const mockToastService = {
    error: jasmine.createSpy('error'),
    warning: jasmine.createSpy('warning'),
    success: jasmine.createSpy('success')
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [{
        provide: CarService,
        useValue: mockService
      }, {
        provide: RoleService,
        useValue: MockRoleService
      }, {
        provide: ConnectionBackend,
        useClass: MockBackend
      }, {
        provide: RequestOptions,
        useClass: BaseRequestOptions
      }, CarService,
      Http,
      {
        provide: ToastrService,
        useValue: mockToastService
      }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call logout', () => {
    spyOn(component, 'logout').and.callThrough();
    component.logout();
    expect(component.logout).toHaveBeenCalled();
  });

});
