import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsaleComponent } from './carsale.component';
import { MockRoleService } from '../MockServices/mock-role.service';
import { RoleService } from '../AuthenticationServices/role.service';
import { CarService } from '../car.service';
// import { MockCarService } from '../MockServices/mockCar.service';
import { ConnectionBackend, BaseRequestOptions, RequestOptions, Http } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { AuthService } from '../AuthenticationServices/auth.service';
import { MockAuthService } from '../MockServices/mock-auth.service';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { mockService } from '../MockServices/mockCar.service';
import { ToastrService } from 'ngx-toastr';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

describe('CarsaleComponent', () => {
  let component: CarsaleComponent;
  let fixture: ComponentFixture<CarsaleComponent>;

  const mockToastService = {
    error: jasmine.createSpy('error'),
    warning: jasmine.createSpy('warning'),
    success: jasmine.createSpy('success'),
    info: jasmine.createSpy('info')
  };

  beforeEach((() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        ProgressSpinnerModule
      ],
      declarations: [ CarsaleComponent ],
      providers: [{
        provide: CarService,
        useValue: mockService
      }, {
        provide: ConnectionBackend,
        useClass: MockBackend
      }, {
        provide: RequestOptions,
        useClass: BaseRequestOptions
      },
      Http, CarService,
      {
        provide: ToastrService,
        useValue: mockToastService
      }]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    localStorage.setItem('id', '1');
    fixture = TestBed.createComponent(CarsaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have id in localstorage', () => {
    expect(localStorage.getItem('id')).toEqual('1');
  });

  it('should show all information about the carsale', () => {
    expect(component.carSaleInfo).toBeDefined();
  });

  it('should have carsale ready', () => {
    component.carSaleReady = true;
    expect(component.carSaleReady).toBeTruthy();
  });

  it('should call sold', () => {
    spyOn(component, 'sold').and.callThrough();
    component.sold(1);
    expect(component.sold).toHaveBeenCalled();
  });

  it('should call getCarsaleCars', () => {
    spyOn(component, 'getCarsaleCars').and.callThrough();
    component.getCarsaleCars();
    expect(component.getCarsaleCars).toHaveBeenCalled();
  });
  it('should call searchForRegNum() with regNum', () => {
    spyOn(component, 'searchForRegNum').and.callThrough();
    component.searchRegNum = 'NT132';
    component.searchString = '(carsaleId:3)';
    component.searchForRegNum();
    expect(component.searchForRegNum).toHaveBeenCalled();
  });
  it('should call searchForRegNum() without regNum', () => {
    spyOn(component, 'searchForRegNum').and.callThrough();
    component.searchRegNum = '';
    component.searchString = '(carsaleId:3)';
    component.searchForRegNum();
    expect(component.searchForRegNum).toHaveBeenCalled();
  });
  it('should call getAllCars()', () => {
    spyOn(component, 'getAllCars').and.callThrough();
    component.carsaleTerm = '(carsaleId:3)';
    component.getAllCars();
    expect(component.getAllCars).toHaveBeenCalled();
  });
  it('should call getCarDetailPage()', () => {
    spyOn(component, 'getCarDetailPage').and.callThrough();
    const car = {id: 1};
    component.getCarDetailPage(car);
    expect(component.getCarDetailPage).toHaveBeenCalled();
  });
});
