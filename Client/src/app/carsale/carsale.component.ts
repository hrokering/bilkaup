import { Component, OnInit } from '@angular/core';
import { UserInfoDTO, CarSaleDetailDTO, CarCardDTO } from '../app.models';
import { CarService } from '../car.service';
import { HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-carsale',
  templateUrl: './carsale.component.html',
  styleUrls: ['./carsale.component.css']
})
export class CarsaleComponent implements OnInit {

  public carSaleInfo: CarSaleDetailDTO;
  public carSaleReady: boolean;
  public showOptions: boolean;
  public showID: number;
  public sort = 'iD';
  public carsaleTerm = '(carsaleId:' + localStorage.getItem('id') + ')';
  public searchRegNum: string;
  public searchString: string;

  constructor(public service: CarService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.carSaleReady = false;
    this.carSaleInfo = new CarSaleDetailDTO();
    this.showOptions = false;
    this.showID = 0;
    this.searchString = this.carsaleTerm;
    this.searchRegNum = '';

    this.service.getCarSaleDetail(+localStorage.getItem('id')).subscribe(
      carSale => {
        this.carSaleInfo = carSale;
        this.carSaleReady = true;
        this.getCarsaleCars();
      }, err => {
        this.toastr.error('Tókst ekki að sækja upplýsingar um bílasölu!', 'Error!');
    });
  }

  // Search for a registration number
  searchForRegNum() {
    if (this.searchRegNum.length > 0) {
      this.searchString += ('AND(licenceNumber:' + this.searchRegNum + ')');
    } else {
      this.searchString = this.carsaleTerm;
    }
    this.getCarsaleCars();
  }

  // Gets all cars
  getAllCars() {
    this.searchString = this.carsaleTerm;
    this.searchRegNum = '';
    this.getCarsaleCars();
  }

  // Routs to car detail page
  getCarDetailPage(car) {
    this.router.navigate(['../car/' + car.id]);
  }

  // Gets all cars for carsale
  getCarsaleCars () {
    this.service.getFilteredCars(this.sort, this.searchString).subscribe(
      cars => {
        this.carSaleInfo.cars = cars;
      }, err => {
        this.toastr.error('Tóks ekki að sækja bíla!', 'Error!');
      }
    );
  }

  // Sells a car
  sold(serialNum: number) {
    this.service.sellCar(serialNum).subscribe(
      res => {
        // TODO!!! does not always work???
        this.getCarsaleCars();
        this.toastr.success('Bíllinn hefur verið seldur!', 'Aðgerð tókst!');
      }, err => {
        this.toastr.error('Tókst ekki að selja bílinn', 'Error!');
    });
  }

  // Returns a number with thousands points
  toThousand(num) {
    if (num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
    return null;
  }
}
