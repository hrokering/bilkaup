import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarService } from '../car.service';
import { CarSaleViewModel } from '../app.models';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register-car-sale',
  templateUrl: './register-car-sale.component.html',
  styleUrls: ['./register-car-sale.component.css']
})
export class RegisterCarSaleComponent implements OnInit {

  newCarSale: CarSaleViewModel;
  myForm: FormGroup;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: CarService,
    private fb: FormBuilder,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      name: new FormControl('', Validators.required),
      ssn: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      phoneNum: new FormControl('', Validators.required),
      address: new FormControl(),
      webpage: new FormControl()
    });
  }

  // Submits a carsale registration form
  submit() {
    console.log(this.myForm.value);

    const reply = this.service.addCarSale(this.myForm.value).subscribe(
      carSale => {
        this.toastr.success('Umsókn þín hefur verið mótekin!', 'Aðgerð tókst!');
        this.router.navigate(['../']);
      }, err => {
        this.toastr.error('Því miður fór eitthvað úrskeiðis og ekki náðist að móttaka umsókn þína. Endilega reyndu aftur', 'Error!');
    });
  }

}
