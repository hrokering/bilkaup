import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router, ActivatedRouteSnapshot } from '@angular/router';
import { CarService } from '../car.service';
import { CarSaleDTO, LoginDTO, RegisterViewModel } from '../app.models';
import { RoleService } from '../AuthenticationServices/role.service';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import * as decode from 'jwt-decode';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

export class AdminComponent implements OnInit {

  public carSales: CarSaleDTO[];
  private token: string;
  private access: boolean;
  public  waitingCount: number;
  private activeCount: number;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: CarService,
    private roleService: RoleService,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.carSales = new Array<CarSaleDTO>();
    this.waitingCount = 0;
    this.activeCount = 0;

    this.service.getAdminCarSales().subscribe(
      carSales => {
        this.carSales = carSales;
        // Might be done in a better way in HTML
        this.getWaitingCount();
      }, err => {
        console.log('Could not load waiting carsales');
    });
  }

  // Gets the number of carsales waiting to be accepted
  getWaitingCount() {
    for (const value of this.carSales) {
      if (value.accepted === false) {
          this.waitingCount++;
      } else if (value.accepted === true && value.active === true) {
        this.activeCount++;
      }
    }
  }

  // Registers a carsale but does not give it access to Bilkaup
  registerCarSale(carSale) {
    const regInfo = new RegisterViewModel();
      regInfo.email = carSale.email;
      regInfo.role = 'Carsale';

    const reply = this.service.registerCarSale(carSale).subscribe(
      carSales => {
        console.log('Carsale was registered');
      }, err => {
        console.log('Could not register carSale');
    });
  }

  // Sets our carsales
  setCarsales(carsales, toastrMessage1, toastrMessage2) {
    this.carSales = carsales;
        this.toastr.success(toastrMessage1, toastrMessage2);
  }

  // Gives a carsale access to Bilkaup
  acceptCarSale(carSale) {
    // moved all functionality to registerCarSale because functions were not in sync
    console.log(carSale);
    const regInfo = new RegisterViewModel();
      regInfo.email = carSale.email;
      regInfo.role = 'Carsale';
    this.service.registerCarSale(regInfo).subscribe(
      carSales => {
        this.setCarsales(carSales, 'Bílasölu hefur verið veitt aðgangur', 'Aðgerð tókst!');
      }, err => {
        this.toastr.error('Ekki tókst að veita bílasölu aðgang', 'Error!');
    });
  }

  // Revokes a carsale account
  revokeCarSale(carSale) {
    const reply = this.service.revokeCarSale(carSale).subscribe(
      carSales => {
        this.setCarsales(carSales, 'Aðgangur bílasölu hefur verið fjarlægður', 'Aðgerð tókst!');
      }, err => {
        this.toastr.error('Ekki tókst að fjarlægja aðgang bílasölu', 'Error!');
    });
  }

  // Denies a carsale account
  denyCarSale(carSale) {
    this.service.denyCarSale(carSale).subscribe(
      carSales => {
        this.setCarsales(carSales, 'Bílasölu hefur verið hafnað', 'Aðgerð tókst!');
      }, err => {
        this.toastr.error('Ekki tókst að neita bílasölu um aðgang', 'Error!');
        console.log('could not delete carSale');
    });
  }
}
