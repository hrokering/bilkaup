import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { CarService } from './car.service';
import { CarDTO } from './app.models';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { AuthService } from './AuthenticationServices/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    static logout: any;
    constructor(private router: Router,
        private service: CarService,
        private toastr: ToastrService) { }

    public loggedIn: boolean;
    public myPage: string;

    ngOnInit() {
        this.loggedIn = false;
        const token = localStorage.getItem('token');
        this.myPage = localStorage.getItem('myPage');
        if (token) {
            console.log('has token');
            this.loggedIn = true;
        }
    }

    public logout() {
        console.log('in logout');
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        localStorage.removeItem('id');
        localStorage.removeItem('myPage');
        this.loggedIn = false;
        this.myPage = '';
        const reply = this.service.logout().subscribe(
            logoutInfo => {
                this.toastr.success('Þú ert nú útskráður.', 'Aðgerð tókst!');
            }
        );
        this.router.navigate(['']);
    }
}
